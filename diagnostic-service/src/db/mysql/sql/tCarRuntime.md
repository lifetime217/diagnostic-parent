
getCarRuntimeInfo1ByStartEndId
===
* 通过car_startend_id找充电过程中的最后一条记录

select * from t_car_runtime where car_startend_id =#car_startend_id# order by seq desc limit 1


getCarRuntimeInfo2ByStartEndId
===
* 通过car_startend_id找充电过程中的倒数第二条的记录

select * from t_car_runtime where car_startend_id =#car_startend_id# order by seq desc limit 1,1

