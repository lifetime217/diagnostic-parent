
getEndIdByCarVh
===
* 通过车辆标识符查询最后一次充电前状态的数据id

select id from t_car_startend where car_vh = #car_vh# order by add_time_start desc limit 1

getCarRuntimeSeqByCarVh
===
* 通过充电过程中的vh找充电前最后一条的runtimeseq值

select runtime_seq from t_car_startend where car_vh = #car_vh# order by id desc limit 1

updateCarRuntimeSeqBySeq
===
* 通过充电过程中的seq更新充电前最后一条runtimeseq值

update t_car_startend set runtime_seq = #runtime_seq# order by id desc limit 1

