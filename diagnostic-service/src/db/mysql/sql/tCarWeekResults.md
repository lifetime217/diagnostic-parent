
getCarWeekResultByCarVh
===
* 通过车辆标识符查询carweekresult数据

select * from t_car_week_results where car_vh = #car_vh# order by id desc limit 1


getMonthChrgcapByAddYearmon
===
* 通过车辆标识符以及时间查询carweekresult中的某月充电总容量累计值

select sum(chrg_cap_total) from t_car_week_results where car_vh = #car_vh# and add_yearmon =#add_yearmon#


getMonthChrgferyByAddYearmon
===
* 通过车辆标识符以及时间查询carweekresult中的某月充电频数累计值

select sum(chrg_dc_fery) from t_car_week_results where car_vh = #car_vh# and add_yearmon =#add_yearmon#

getMonthBatCapacityByAddYearmon
===
* 通过车辆标识符以及时间查询carweekresult中的某月电池包容量

select avg(capacity_bat) from t_car_week_results where car_vh = #car_vh# and add_yearmon =#add_yearmon#