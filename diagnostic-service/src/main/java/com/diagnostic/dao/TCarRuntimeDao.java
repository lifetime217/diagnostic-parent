package com.diagnostic.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import com.diagnostic.entity.TCarRuntime;

public interface TCarRuntimeDao extends BaseMapper<TCarRuntime> {


	/**
	 * 通过car_startend_id找充电过程中的最后一条记录
	 * 
	 * @param car_startend_id
	 * @return
	 */
	public TCarRuntime getCarRuntimeInfo1ByStartEndId(@Param("car_startend_id") int car_startend_id);

	/**
	 * 通过car_startend_id找充电过程中的倒数第二条的记录
	 * 
	 * @param car_startend_id
	 * @return
	 */
	public TCarRuntime getCarRuntimeInfo2ByStartEndId(@Param("car_startend_id") int car_startend_id);


}
