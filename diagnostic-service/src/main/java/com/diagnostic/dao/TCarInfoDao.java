package com.diagnostic.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import com.diagnostic.entity.TCarInfo;

public interface TCarInfoDao extends BaseMapper<TCarInfo>{
	
	/**
	 * 通过carVh找温度报警的最新记录
	 * 
	 * @param carVh
	 * @return
	 */
	public TCarInfo getCarInfoByCarVh(@Param("car_vh")String carVh);
}
