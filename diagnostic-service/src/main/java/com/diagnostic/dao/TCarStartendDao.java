package com.diagnostic.dao;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import com.diagnostic.entity.TCarStartend;

public interface TCarStartendDao extends BaseMapper<TCarStartend> {

	/**
	 * 通过车辆标识符查询最后一次充电前状态的数据id
	 * 
	 * @param cardVh
	 * @return
	 */
	public int getEndIdByCarVh(@Param("car_vh") String carVh);

	/**
	 * 通过充电过程中的vh找充电前最后一条的runtimeseq值
	 * 
	 * @param vh
	 * @return
	 */
	public Integer getCarRuntimeSeqByCarVh(@Param("car_vh") String vh);

	/**
	 * 通过充电过程中的seq更新充电前最后一条runtimeseq值
	 * 
	 * @param vh
	 * @return
	 */
	public void updateCarRuntimeSeqBySeq(@Param("runtime_seq") Integer runtime_seq);

}
