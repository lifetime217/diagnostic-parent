package com.diagnostic.dao;

import java.math.BigDecimal;

import org.beetl.sql.core.annotatoin.Param;
import org.beetl.sql.core.mapper.BaseMapper;

import com.diagnostic.entity.TCarWeekResults;

public interface TCarWeekResultsDao extends BaseMapper<TCarWeekResults> {
	
	/**
	 * 通过车辆标识符查询carweekresult数据
	 * 
	 * @param carVh
	 * @return
	 */
	public TCarWeekResults getCarWeekResultByCarVh(@Param("car_vh") String vh);
	/**
	 * 通过车辆标识符以及时间查询carweekresult中的某月充电总容量累计值
	 * 
	 * @param carVh，add_yearmon
	 * @return
	 */
	public BigDecimal  getMonthChrgcapByAddYearmon(@Param("car_vh")String car_vh,@Param("add_yearmon")int add_yearmon);
	/**
	 * 通过车辆标识符以及时间查询carweekresult中的某月充电频数累计值
	 * 
	 * @param carVh，add_yearmon
	 * @return
	 */
	public BigDecimal  getMonthChrgferyByAddYearmon(@Param("car_vh")String car_vh,@Param("add_yearmon")int add_yearmon);
	/**
	 * 通过车辆标识符以及时间查询carweekresult中的某月电池包容量
	 * 
	 * @param carVh，add_yearmon
	 * @return
	 */
	public BigDecimal  getMonthBatCapacityByAddYearmon(@Param("car_vh")String car_vh,@Param("add_yearmon")int add_yearmon);
}
