package com.diagnostic.service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.diagnostic.dao.TCarInfoDao;
import com.diagnostic.dao.TCarRuntimeDao;
import com.diagnostic.dao.TCarStartendDao;
import com.diagnostic.entity.TCarInfo;
import com.diagnostic.entity.TCarRuntime;

@Service
public class TCarRuntimeService implements ITCarRuntimeService {

	private static final Logger log = LoggerFactory.getLogger(TCarRuntimeService.class);
	@Autowired
	private TCarStartendDao carStartendDao;

	@Autowired
	private TCarRuntimeDao carRuntimeDao;

	@Autowired
	private TCarInfoDao carInfoDao;

	@Override
	public String save(TCarRuntime info) {
		long time = System.currentTimeMillis();
		log.info("=> 车辆充电中数据存储...");
		int oneHour = 60 * 60;
		// 获取当前车辆标识符
		String vh = info.getCarVh();
		// 根据车辆标识符拿到最近一次充电行为（充电前）的id
		int carStartEndId = carStartendDao.getEndIdByCarVh(vh);
		// 设置运行时数据与充电行为数据之前的表id关联
		info.setCarStartendId(carStartEndId);
		// 通过开始充电表里的id找充电过程中的最后一次的充电过程中的序号
		TCarRuntime k_1 = carRuntimeDao.getCarRuntimeInfo1ByStartEndId(carStartEndId);

		if (k_1 != null) {
			// 计算本次充电容量
			int s = info.getDataTime() - k_1.getDataTime();
			// 当次的充电量=DcChrgCap + bat_curr * △t
			BigDecimal currentDcChrgCap = k_1.getDcChrgCap().add(info.getBatCurr().multiply(new BigDecimal(String.valueOf(s * 1.0000 / oneHour))));
			info.setDcChrgCap(currentDcChrgCap);
		} else {
			info.setDcChrgCap(new BigDecimal(0));
		}

		// 通过温度最值计算极差
		BigDecimal tempMaxMin = info.getTempMax().subtract(info.getTempMin());
		info.setTempMaxMin(tempMaxMin);

		// 通过温度最值计算平均值
		BigDecimal tempAvg = (info.getTempMax().add(info.getTempMin())).divide(new BigDecimal(2));
		info.setTempAvg(tempAvg);
		info.setAddTime((int) Instant.now().getEpochSecond());

		// 计算info_time_day info_time_second
		int datatime = info.getDataTime();
		Date date = new Date((long) datatime * 1000);
		SimpleDateFormat formatter1 = new SimpleDateFormat("yyyyMMdd");
		int day = Integer.parseInt(formatter1.format(date));
		info.setInfTimeDay(day);

		// 计算info_time_second
		SimpleDateFormat formatter2 = new SimpleDateFormat("HHmmss");
		int second = Integer.parseInt(formatter2.format(date));
		info.setInfTimeSecond(second);

		// 计算seq
		Integer runtimeseq = carStartendDao.getCarRuntimeSeqByCarVh(vh);
		info.setSeq(runtimeseq);
		runtimeseq = runtimeseq + 1;
		carStartendDao.updateCarRuntimeSeqBySeq(runtimeseq);// 更新充电前记录中的seq值
		carRuntimeDao.insert(info);// 插入新的充电中记录

		int startendid = info.getCarStartendId();
		alertTempState(vh, startendid);
		log.info("=> 充电中数据存储 cost time:" + (System.currentTimeMillis() - time));
		return null;

	}

	/**
	 * 预警车辆温度状态
	 * 
	 * @param carVh
	 */
	private void alertTempState(String carVh, int startendid) {
		// 通过vh查看t_car_info表里有没有数据
		TCarInfo info = carInfoDao.getCarInfoByCarVh(carVh);
		// 通过getState方法获取温度预警值
		String state = getState(startendid);
		if (info == null) {
			info = new TCarInfo();
			info.setCarVh(carVh);
			info.setUpdateTime((int) (System.currentTimeMillis() / 1000));
			info.setChrgTempState(state);
			carInfoDao.insert(info);
		} else {
			info.setChrgTempState(state);
			info.setUpdateTime((int) (System.currentTimeMillis() / 1000));
			carInfoDao.updateTemplateById(info);
		}
	}


	public String getState(int startendid) {
		log.info("获取温度状态");
		// 高温实时算法
		/* 温度阈值 */
		BigDecimal temp1 = new BigDecimal("40");
		BigDecimal temp2 = new BigDecimal("45");
		BigDecimal temp3 = new BigDecimal("50");
		BigDecimal deta_temp1 = new BigDecimal("0.5");
		BigDecimal deta_temp2 = new BigDecimal("1");

		TCarRuntime carruntime1 = carRuntimeDao.getCarRuntimeInfo1ByStartEndId(startendid);
		BigDecimal tempmax1 = carruntime1.getTempMax();
		BigDecimal cellvoltmax1 = carruntime1.getCellVoltMax();
		int s1 = carruntime1.getDataTime();

		TCarRuntime carruntime2 = carRuntimeDao.getCarRuntimeInfo2ByStartEndId(startendid);
		if (carruntime2 != null) {
			BigDecimal tempmax2 = carruntime2.getTempMax();
			BigDecimal cellvoltmax2 = carruntime2.getCellVoltMax();
			int s2 = carruntime2.getDataTime();
			int seq = carruntime2.getSeq();

			/* 最高单体温度40度以下安全 */
			if (tempmax1.compareTo(temp1) < 0) {
				return ("安全");
			}
			/* 最高单体温度40度到45度，且温升达到0.5/s 温升过快；或者最高单体电压产生压降且下降值超过初始电压的15% */
			else if (tempmax1.compareTo(temp2) < 0 && tempmax1.compareTo(temp1) >= 0
					&& tempmax1.subtract(tempmax2).divide(new BigDecimal((s1 - s2)), RoundingMode.HALF_UP).abs().compareTo(deta_temp1) > 0) {
				return ("温升过快");
			}
			/* 最高单体温度在45度到50度 温度过高警告 */
			else if (tempmax1.compareTo(temp3) < 0 && tempmax1.compareTo(temp2) >= 0) {
				return ("温度过高");
			}
			/* 最高单体温度50℃以上，且监测点的温升速率达到1 ℃/s 热失控热失控报警 */
			else if (tempmax1.compareTo(temp1) >= 0 && tempmax1.subtract(tempmax2).divide(new BigDecimal((s1 - s2)), RoundingMode.HALF_UP).abs().compareTo(deta_temp2) > 0) {
				return ("热失控");
			}
			/* 最高单体电压产生压降且下降值超过初始电压的25% 热失控 */
			if (cellvoltmax1.subtract(cellvoltmax2).compareTo(new BigDecimal(0)) < 0
					&& cellvoltmax2.subtract(cellvoltmax1).compareTo(cellvoltmax2.multiply(new BigDecimal(0.25))) > 0) {
				return ("热失控");
			}
			/* 温升过快:最高单体电压产生压降且下降值超过初始电压的15% */
			else if (cellvoltmax1.subtract(cellvoltmax2).compareTo(new BigDecimal(0)) < 0
					&& cellvoltmax2.subtract(cellvoltmax1).compareTo(cellvoltmax2.multiply(new BigDecimal(0.15))) > 0
					&& cellvoltmax2.subtract(cellvoltmax1).compareTo(cellvoltmax2.multiply(new BigDecimal(0.25))) < 0) {
				return ("温升过快");
			}
		} else {
			return ("监测中");
		}
		return null;
	}

}
