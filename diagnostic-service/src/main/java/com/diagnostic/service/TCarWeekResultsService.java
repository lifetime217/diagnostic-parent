package com.diagnostic.service;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.alibaba.dubbo.config.annotation.Service;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.diagnostic.dao.TCarInfoDao;
import com.diagnostic.dao.TCarWeekResultsDao;
import com.diagnostic.entity.TCarInfo;
import com.diagnostic.entity.TCarWeekResults;

@Service
public class TCarWeekResultsService implements ITCarWeekResultsService {
	
	private static final Logger log = LoggerFactory.getLogger(TCarWeekResultsService.class);
	
	private static final SimpleDateFormat format = new SimpleDateFormat("yyyyMM");
	
	@Autowired
	private TCarWeekResultsDao carWeekResultsDao;

	@Autowired
	private TCarInfoDao carInfoDao;

	/**
	 * 根据车辆标识符 获得充电行为综合评价
	 */
	@Override
	public List<BigDecimal> chrgBehaviorEstimation(String vh) {
		long time = System.currentTimeMillis();
		log.info("=> start chrg_behavior_estimation...");
		TCarWeekResults weekresults = carWeekResultsDao.getCarWeekResultByCarVh(vh);
		List<BigDecimal> data = new ArrayList<BigDecimal>();
		// 快充频率得分
		BigDecimal chrgDcFeryScore = weekresults.getChrgDcFeryScore();
		data.add(chrgDcFeryScore);
		// 正常结束频率得分
		BigDecimal chrgEndFeryScore = weekresults.getChrgEndFeryScore();
		data.add(chrgEndFeryScore);
		// 充电时段得分
		BigDecimal chrgTimeScore = weekresults.getChrgTimeScore();
		data.add(chrgTimeScore);
		// 放电深度得分
		BigDecimal dschDodScore = weekresults.getDschDodScore();
		data.add(dschDodScore);
		// 过放电频率得分
		BigDecimal dschOverFeryScore = weekresults.getDschOverFeryScore();
		data.add(dschOverFeryScore);
		log.debug("chrg_behavior_estimation cost time:" + (System.currentTimeMillis() - time));
		return data;
	}

	@Override
	public List<String> states(String vh) {
		long time = System.currentTimeMillis();
		log.info("=> states...");
		TCarWeekResults weekresults = carWeekResultsDao.getCarWeekResultByCarVh(vh);
		List<String> data = new ArrayList<String>();
		String soc = weekresults.getCapacityBat().toString();
		TCarInfo carInfo = carInfoDao.getCarInfoByCarVh(vh);
		data.add(soc);
		if (carInfo != null) {
			String tempstate = carInfo.getChrgTempState();
			data.add(tempstate);
		} else {
			data.add("监测中");
		}
		log.debug("states cost time:" + (System.currentTimeMillis() - time));
		return data;
	}

	@Override
	public JSONObject voltFreyModulecell(String vh) {
		JSONObject res = new JSONObject();
		List<String> list = new ArrayList<>();
		JSONObject obj1 = new JSONObject();
		JSONObject obj2 = new JSONObject();
		JSONObject obj3 = new JSONObject();
		JSONObject obj4 = new JSONObject();
		JSONObject obj5 = new JSONObject();
		JSONArray arr = new JSONArray();
		long time = System.currentTimeMillis();
		log.info("=> volt_modulecell...");
		TCarWeekResults weekresults = carWeekResultsDao.getCarWeekResultByCarVh(vh);
		// 获取频率第1的箱号单体号。电池信息
		String modulecell_volt_max_no1 = weekresults.getModuleVoltMaxNo1() + "箱" + weekresults.getCellVoltMaxNo1() + "号";
		String modulecell_volt_max_no2 = weekresults.getModuleVoltMaxNo2() + "箱" + weekresults.getCellVoltMaxNo2() + "号";
		String modulecell_volt_max_no3 = weekresults.getModuleVoltMaxNo3() + "箱" + weekresults.getCellVoltMaxNo3() + "号";
		String modulecell_volt_max_no4 = weekresults.getModuleVoltMaxNo4() + "箱" + weekresults.getCellVoltMaxNo4() + "号";
		list.add("'" + modulecell_volt_max_no1 + "'");
		list.add("'" + modulecell_volt_max_no2 + "'");
		list.add("'" + modulecell_volt_max_no3 + "'");
		list.add("'" + modulecell_volt_max_no4 + "'");
		list.add("'其他'");
		// 获取频率第1的频数
		BigDecimal volt_max_frey_no1 = weekresults.getVoltMaxFreyNo1();
		// 获取频率第2的频数
		BigDecimal volt_max_frey_no2 = weekresults.getVoltMaxFreyNo2();
		// 获取频率第3的频数
		BigDecimal volt_max_frey_no3 = weekresults.getVoltMaxFreyNo3();
		// 获取频率第4的频数
		BigDecimal volt_max_frey_no4 = weekresults.getVoltMaxFreyNo4();
		obj1.put("name", modulecell_volt_max_no1);
		obj1.put("value", volt_max_frey_no1);
		obj2.put("name", modulecell_volt_max_no2);
		obj2.put("value", volt_max_frey_no2);
		obj3.put("name", modulecell_volt_max_no3);
		obj3.put("value", volt_max_frey_no3);
		obj4.put("name", modulecell_volt_max_no4);
		obj4.put("value", volt_max_frey_no4);
		obj5.put("name", "其他");
		obj5.put("value", new BigDecimal(1).subtract(volt_max_frey_no1).subtract(volt_max_frey_no2).subtract(volt_max_frey_no3).subtract(volt_max_frey_no4));

		arr.add(obj1);
		arr.add(obj2);
		arr.add(obj3);
		arr.add(obj4);
		arr.add(obj5);
		log.info("volt_modulecell cost time:" + (System.currentTimeMillis() - time));
		// 封装piedata，legenddata
		res.put("legendData", list);
		res.put("pieData", arr);
		return res;
	}


	@Override
	public JSONObject tempFreyNum(String vh) {
		JSONObject res = new JSONObject();
		List<String> list = new ArrayList<>();
		JSONObject obj1 = new JSONObject();
		JSONObject obj2 = new JSONObject();
		JSONObject obj3 = new JSONObject();
		JSONObject obj4 = new JSONObject();
		JSONObject obj5 = new JSONObject();
		JSONArray arr = new JSONArray();
		long time = System.currentTimeMillis();
		log.info("=> temp_num...");
		TCarWeekResults weekresults = carWeekResultsDao.getCarWeekResultByCarVh(vh);
		// 获取最高温度频率第1的编号
		String temp_max_no1 = weekresults.getTempMaxNo1() + "号";
		String temp_max_no2 = weekresults.getTempMaxNo2() + "号";
		String temp_max_no3 = weekresults.getTempMaxNo3() + "号";
		String temp_max_no4 = weekresults.getTempMaxNo4() + "号";

		// 获取频率第1的频数
		BigDecimal temp_max_frey_no1 = weekresults.getVoltMaxFreyNo1();
		BigDecimal temp_max_frey_no2 = weekresults.getVoltMaxFreyNo2();
		BigDecimal temp_max_frey_no3 = weekresults.getVoltMaxFreyNo3();
		BigDecimal temp_max_frey_no4 = weekresults.getVoltMaxFreyNo4();

		list.add("'" + temp_max_no1 + "'");
		list.add("'" + temp_max_no2 + "'");
		list.add("'" + temp_max_no3 + "'");
		list.add("'" + temp_max_no4 + "'");
		list.add("'其他'");

		obj1.put("name", temp_max_no1);
		obj1.put("value", temp_max_frey_no1);
		obj2.put("name", temp_max_no2);
		obj2.put("value", temp_max_frey_no2);
		obj3.put("name", temp_max_no3);
		obj3.put("value", temp_max_frey_no3);
		obj4.put("name", temp_max_no4);
		obj4.put("value", temp_max_frey_no4);
		obj5.put("name", "其他");
		obj5.put("value", new BigDecimal(1).subtract(temp_max_frey_no1).subtract(temp_max_frey_no2).subtract(temp_max_frey_no3).subtract(temp_max_frey_no4));
		arr.add(obj1);
		arr.add(obj2);
		arr.add(obj3);
		arr.add(obj4);
		arr.add(obj5);
		log.debug("temp_numy cost time:" + (System.currentTimeMillis() - time));
		// 封装piedata，legenddata
		res.put("legendData", list);
		res.put("pieData", arr);
		return res;
	}

	/**
	 * 月充电量/充电次数
	 */
	@Override
	public JSONObject monChrg(String vh) {
		JSONObject res = new JSONObject();
		long time = System.currentTimeMillis();
		log.info("=> monChrgcap...");
		List<BigDecimal> dataChrgcap = new ArrayList<BigDecimal>();
		List<BigDecimal> dataChrgfery = new ArrayList<BigDecimal>();
		Calendar calendar = Calendar.getInstance();
		String currentYear = calendar.get(Calendar.YEAR) + "";
		String currentMonth = calendar.get(Calendar.MONTH) + 1 + "";
		Date date = null;
		Integer yearmon = null;
		try {
			for (int i = 1; i < Integer.parseInt(currentMonth) + 1; i++) {
				date = format.parse(currentYear + i);
				String dateString = format.format(date);
				yearmon = Integer.valueOf(dateString);
				BigDecimal monthChrgcap = carWeekResultsDao.getMonthChrgcapByAddYearmon(vh, yearmon);
				if (monthChrgcap != null) {
					dataChrgcap.add(monthChrgcap);
				} else {
					dataChrgcap.add(new BigDecimal(0.00));
				}
				BigDecimal monthChrgfery = carWeekResultsDao.getMonthChrgferyByAddYearmon(vh, yearmon);
				if (monthChrgfery != null) {
					dataChrgfery.add(monthChrgfery);
				} else {
					dataChrgfery.add(new BigDecimal(0.00));
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e.fillInStackTrace());
		}
		log.info("monChrgcap cost time:" + (System.currentTimeMillis() - time));
		res.put("chrgCap", dataChrgcap);//某月充电总容量累计值
		res.put("chrgFery", dataChrgfery);//某月充电频数累计值
		return res;
	}


	/**
	 * 某月电池包容量
	 */
	@Override
	public List<BigDecimal> batCapacity(String vh) {
		long time = System.currentTimeMillis();
		log.info("=> batCapacity...");
		List<BigDecimal> data = new ArrayList<BigDecimal>();
		Calendar calendar = Calendar.getInstance();
		String currentYear = calendar.get(Calendar.YEAR) + "";
		String currentMonth = calendar.get(Calendar.MONTH) + 1 + "";
		Date date = null;
		Integer yearmon = null;
		try {
			for (int i = 1; i < Integer.parseInt(currentMonth) + 1; i++) {
				date = format.parse(currentYear + i);
				String dateString = format.format(date);
				yearmon = Integer.valueOf(dateString);
				BigDecimal monthcap = carWeekResultsDao.getMonthBatCapacityByAddYearmon(vh, yearmon);//某月电池包容量
				if (monthcap != null) {
					data.add(monthcap);
				} else {
					data.add(new BigDecimal(0.00));
				}
			}
		} catch (ParseException e) {
			log.error(e.getMessage(), e.fillInStackTrace());
		}
		log.info("batCapacity cost time:" + (System.currentTimeMillis() - time));
		return data;
	}

}
