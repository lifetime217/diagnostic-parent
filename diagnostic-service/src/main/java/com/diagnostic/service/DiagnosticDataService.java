package com.diagnostic.service;

import java.math.BigDecimal;

import org.beetl.sql.core.db.KeyHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.dubbo.config.annotation.Service;
import com.diagnostic.dao.TCarStartendDao;
import com.diagnostic.entity.TCarRuntime;
import com.diagnostic.entity.TCarStartend;

@Service
public class DiagnosticDataService implements IDiagnosticDataService {
	
	private static final Logger log = LoggerFactory.getLogger(TCarRuntimeService.class);
	
	@Autowired
	private TCarStartendDao carStartendDao;
	
	@Autowired
	private TCarRuntimeService carRuntimeService;

	/**
	 * 接收充电开始的数据
	 */
	@Override
	@Transactional
	public Long chrgStart(TCarStartend info) {
		log.info("=> chrgStart ...");
		try {
			info.setAddTimeStart((int)(System.currentTimeMillis()/1000));
			info.setBatChrgMax(BigDecimal.ZERO);
			info.setRuntimeSeq(1);
			KeyHolder holder = carStartendDao.insertReturnKey(info);
			Object id = holder.getKey();
			return (Long) id;
		} catch (Exception e) {
			log.error(e.getMessage(),e.fillInStackTrace());
			throw new RuntimeException();
		}
		
	}

	/**
	 * 接收充电结束的数据
	 */
	@Override
	@Transactional
	public void chrgEnd(TCarStartend info) {
		log.info("=> chrgEnd ...");
		try {
			info.setAddTimeEnd((int)(System.currentTimeMillis()/1000));
		    String vh = info.getCarVh();
			int id = carStartendDao.getEndIdByCarVh(vh);//通过车辆标识符查询最后一次充电前状态的数据id
			info.setId(id);
			carStartendDao.updateTemplateById(info);//更新结果
		} catch (Exception e) {
			log.error(e.getMessage(),e.fillInStackTrace());
			throw new RuntimeException();
		}
		
	}
	
	/**
	 * 接收充电中的数据
	 */
	public void chrgRuntime(TCarRuntime info) {
		log.info("=> chrgRuntime ...");
		carRuntimeService.save(info);
	}
}
