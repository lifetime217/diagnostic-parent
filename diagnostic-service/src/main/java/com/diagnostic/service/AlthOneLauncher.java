package com.diagnostic.service;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

@Component
public class AlthOneLauncher implements InitializingBean {
	private static final Logger log = LoggerFactory.getLogger(AlthOneLauncher.class);

	@Autowired
	private Environment env;

	private Timer timer;

	@Override
	public void afterPropertiesSet() throws Exception {
		timer = new Timer(true);
		timer.scheduleAtFixedRate(new TimerTask() {
			@Override
			public void run() {
				runAlth();
			}
		}, 3000, 600000);
	}

	/**
	 * 
	 *调用算法1
	 */
	protected void runAlth() {
		log.info("开始调用python接口算法1....");
		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost httpPost = new HttpPost(env.getProperty("alth.one.url"));
		JSONObject params = new JSONObject();
		params.put("car_vh", "1G1BL52P7TR115520");
		params.put("start_time", 1534547286);
		params.put("end_time", 1534316340);
		StringEntity stringEntity = new StringEntity(params.toJSONString(), "UTF-8");
		stringEntity.setContentEncoding("UTF-8");
		stringEntity.setContentType("application/json");
		CloseableHttpResponse response = null;
		// 调用Python算法接口
		try {
			httpPost.setEntity(stringEntity);
			response = httpclient.execute(httpPost);
			HttpEntity entity = response.getEntity();
			log.info("状态码：{} , 响应内容：{} ", response.getStatusLine(), EntityUtils.toString(entity));
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage());
		} finally {
			try {
				response.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		log.info("算法1结束");
	}

}
