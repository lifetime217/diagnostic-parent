package com.diagnostic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;


/**
 * @author lifetime
 *
 */
@SpringBootApplication
@DubboComponentScan(basePackages = "com.diagnostic.service")
public class diagnosticServiceStart {
	
	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(diagnosticServiceStart.class, args);
		String[] activeProfiles = ctx.getEnvironment().getActiveProfiles();
		for (String profile : activeProfiles) {
			System.out.println("=========【diagnostic - Server】======== Spring Boot 使用profile为:" + profile);
		}
	}
}
