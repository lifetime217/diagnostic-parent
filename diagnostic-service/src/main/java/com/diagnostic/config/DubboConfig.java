package com.diagnostic.config;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ProtocolConfig;
import com.alibaba.dubbo.config.ProviderConfig;
import com.alibaba.dubbo.config.RegistryConfig;

/**
 * @author lifetime
 *
 */
@Configuration
public class DubboConfig {

	@Autowired
	private Environment env;

	@Bean
	public ApplicationConfig applicationConfig() {
		ApplicationConfig appConfig = new ApplicationConfig();
		try {
			appConfig.setName(env.getProperty("dubbo.application.name", "diagnostic-service-" + InetAddress.getLocalHost().getHostAddress()));
		} catch (UnknownHostException e) {
			appConfig.setName(env.getProperty("dubbo.application.name", "diagnostic-service"));
		}
		appConfig.setQosEnable(false);
		return appConfig;
	}

	@Bean(name = "defaultProvider")
	public ProviderConfig providerConfig(ApplicationConfig appConfig) {

		List<RegistryConfig> addressConfigs = new ArrayList<RegistryConfig>();
		String address = env.getProperty("dubbo.registry.address");
		if (address.indexOf(",") != -1) {
			String[] addresses = address.split(",");
			for (int i = 0; i < addresses.length; i++) {
				addressConfigs.add(new RegistryConfig(addresses[i]));
			}
		} else {
			addressConfigs.add(new RegistryConfig(address));
		}
		ProviderConfig providerConfig = new ProviderConfig();
		providerConfig.setTimeout(env.getProperty("dubbo.provider.timeout", Integer.class, 60000));
		providerConfig.setRetries(env.getProperty("dubbo.provider.retries", Integer.class, 1));
		providerConfig.setDelay(env.getProperty("dubbo.provider.delay", Integer.class, -1));
		providerConfig.setApplication(appConfig);
		providerConfig.setRegistries(addressConfigs);

		List<ProtocolConfig> proConfigs = new ArrayList<ProtocolConfig>();
		String proName = env.getProperty("dubbo.protocol.name", "dubbo");
		int port = env.getProperty("dubbo.protocol.port", Integer.class, 20880);
		if (proName.indexOf(",") != -1) {
			String[] proNames = proName.split(",");
			for (int i = 0; i < proNames.length; i++) {
				proConfigs.add(new ProtocolConfig(proNames[i], port));
			}
		} else {
			proConfigs.add(new ProtocolConfig(proName, port));
		}
		providerConfig.setProtocols(proConfigs);
		return providerConfig;
	}

	@Bean
	public ProtocolConfig dubboProtocolConfig() {
		String proName = env.getProperty("dubbo.protocol.name", "dubbo");
		int port = env.getProperty("dubbo.protocol.port", Integer.class, 20880);
		return new ProtocolConfig(proName, port);
	}

}
