var myChart_1 = echarts.init(document.getElementById('chart_1'));
var myChart_2 = echarts.init(document.getElementById('chart_2'));
var myChart_3 = echarts.init(document.getElementById('chart_3'));
var myChart_4 = echarts.init(document.getElementById('chart_4'));

var chart_1_val = {
	title : {
		text : "充电行为综合评价",
		subtext : ""
	},
	tooltip : {
		trigger : "axis"
	},
	legend : {
		orient : "vertical",
		x : "right",
		y : "bottom",
		data : [ "综合评价" ]
	},
	toolbox : {
		feature : {
			mark : {
				show : true
			},
			dataView : {
				show : true,
				readOnly : false
			},
			restore : {
				show : true
			},
			saveAsImage : {
				show : true
			}
		}
	},
	polar : [ {
		indicator : [ {
			text : "快充频率",
			min : 0,
			max : 10
		}, {
			text : "过放电频率",
			min : 0,
			max : 10
		}, {
			text : "放电深度",
			min : 0,
			max : 10
		}, {
			text : "充电时段",
			min : 0,
			max : 10
		}, {
			text : "正常充电结束频率",
			min : 0,
			max : 10
		} ],
		axisLine : {
			show : true,
			lineStyle : {
				color : "rgb(234, 225, 225)"
			}
		},
		splitLine : {
			show : true
		}
	} ],
	calculable : true,
	series : [ {
		name : "",
		type : "radar",
		data : [ {
			value : [ 6, 8, 7, 4, 7 ],
			name : "综合评价"
		} ]
	} ]
};

var chart_2_val = {
    	title : {
		text : "充电电量及充电次数",
		subtext : ""
	},
    tooltip: {
        trigger: 'axis',
        axisPointer: {
            type: 'cross',
            crossStyle: {
                color: '#999'
            }
        }
    },
    toolbox: {
        feature: {
            dataView: {show: true, readOnly: false},
            magicType: {show: true, type: ['line', 'bar']},
            restore: {show: true},
            saveAsImage: {show: true}
        }
    },
    legend: {
        data:['季度','年']
    },
    xAxis: [
        {
            type: 'category',
            data: ['1月','2月','3月','4月','5月','6月','7月','8月','9月','10月','11月','12月'],
            axisPointer: {
                type: 'shadow'
            }
        }
    ],
    yAxis: [
        {
            type: 'value',
            name: '充电电量',
            min: 0,
            max: 250,
            interval: 50,
            axisLabel: {
                formatter: '{value} '
            }
        },
        {
            type: 'value',
            name: '充电次数',
            min: 0,
            max: 25,
            interval: 5,
            axisLabel: {
                formatter: '{value} '
            }
        }
    ],
    series: [
        {
            name:'季度',
            type:'bar',
            data:[2.0, 4.9, 7.0, 23.2, 25.6, 76.7, 135.6, 162.2, 32.6, 20.0, 6.4, 3.3]
        },
        {
            name:'年',
            type:'line',
            yAxisIndex: 1,
            data:[2.0, 2.2, 3.3, 4.5, 6.3, 10.2, 20.3, 23.4, 23.0, 16.5, 12.0, 6.2]
        }
    ]
};


var chart_3_val = {
    title: {
        text: "最高电压编号频次",
        x: "center"
    },
    tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: "vertical",
        x: "left",
        data: ["1箱10号", "1箱5号", "1箱7号", "12箱45号", "其它"]
    },
    toolbox: {
        feature: {
            mark: {
                show: true
            },
            dataView: {
                show: true,
                readOnly: true
            },
            restore: {
                show: true
            },
            saveAsImage: {
                show: true
            }
        },
        show: false
    },
    calculable: true,
    series: [
        {
            name: "",
            type: "pie",
            radius: "52%",
            center: ["50%", "60%"],
            data: [
                {
                    value: 335,
                    name: "1箱10号"
                },
                {
                    value: 310,
                    name: "1箱5号"
                },
                {
                    value: 234,
                    name: "1箱7号"
                },
                {
                    value: 135,
                    name: "12箱45号"
                },
                {
                    value: 1548,
                    name: "其它"
                }
            ]
        }
    ]
}

var chart_4_val = {
    title: {
        text: "最高温度测点频次",
        x: "center"
    },
    tooltip: {
        trigger: "item",
        formatter: "{a} <br/>{b} : {c} ({d}%)"
    },
    legend: {
        orient: "vertical",
        x: "left",
        data: ["1箱10号", "1箱5号", "1箱7号", "12箱45号", "其它"]
    },
    toolbox: {
        feature: {
            mark: {
                show: true
            },
            dataView: {
                show: true,
                readOnly: true
            },
            restore: {
                show: true
            },
            saveAsImage: {
                show: true
            }
        },
        show: false
    },
    calculable: true,
    series: [
        {
            name: "",
            type: "pie",
            radius: "52%",
            center: ["50%", "60%"],
            data: [
                {
                    value: 335,
                    name: "1箱10号"
                },
                {
                    value: 310,
                    name: "1箱5号"
                },
                {
                    value: 234,
                    name: "1箱7号"
                },
                {
                    value: 135,
                    name: "12箱45号"
                },
                {
                    value: 1548,
                    name: "其它"
                }
            ]
        }
    ]
}




myChart_1.setOption(chart_1_val);
myChart_2.setOption(chart_2_val);
myChart_3.setOption(chart_3_val);
myChart_4.setOption(chart_4_val);



