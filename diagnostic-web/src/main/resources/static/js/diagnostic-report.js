var option = {
			title : {
				text : "充电行为综合评价"
			},
			tooltip : {
				trigger : "axis"
			},
			legend : {
				orient : "vertical",
				x : "right",
				y : "bottom",
				data : [ "-" ]
			},
			toolbox : {
				show : true,
				feature : {
					dataView : {
						readOnly : false
					},
					saveAsImage : {
						show : true
					}
				}
			},
			polar : [ {
				indicator : [ {
					text : "快充频率",
					min : 0,
					max : 10
				}, {
					text : "正常结束频率",
					min : 0,
					max : 10
				}, {
					text : "充电时段",
					min : 0,
					max : 10
				}, {
					text : "放电深度",
					min : 0,
					max : 10
				}, {
					text : "过放电频率",
					min : 0,
					max : 10
				} ],
				type : "circle",
				splitNumber : 5,
				splitArea : {
					show : true
				}
			} ],
			calculable : true,
			series : [ {
				name : "",
				type : "radar",
				data : [{value:estimation,name:"充电行为综合评价"}],
			} ]
		};

		var myChart1 = echarts.init(document
				.getElementById('chongdian_xingwei'), "macarons");
		myChart1.setOption(option);
//第二个图标的option初始化
		option = {
			title : {
				text : "充电电量及充电次数"
			},
			tooltip : {
				trigger : "axis"
			},
			legend : {
				data : [ "充电电量", "充电次数" ]
			},
			toolbox : {
				show : true,
				feature : {
					dataView : {
						readOnly : false
					},
					saveAsImage : {
						show : false
					}
				}
			},
			calculable : true,
			xAxis : [ {
				type : "category",
				axisLine : {
					onZero : false
				},
				data : mon
			} ],
			grid : {
				y : 70
			},
			yAxis : [ {
				type : "value",
				name : "电量"
			}, {
				type : "value",
				name : "次数",
				min : 0,
				max : 20
			} ],
			series : [
					{
						name : "充电电量",
						type : "bar",
						data : chrgCap
					}, {
						name : "充电次数",
						type : "line",
						yAxisIndex : 1,
						data : chrgFery
					} ]
		};
		var myChart2 = echarts.init(document
				.getElementById('chongdian_dianliang'), "macarons");
		myChart2.setOption(option);

		option = {
			title : {
				text : "电池容量曲线"
			},
			tooltip : {
				trigger : "axis"
			},
			legend : {
				data : [ "soc" ]
			},
			toolbox : {
				show : true,
				feature : {
					dataView : {
						readOnly : true
					},
					magicType : {
						type : [ "line", "bar" ],
						show : false
					},
					saveAsImage : {
						show : true
					}
				}
			},
			calculable : true,
			xAxis : [ {
				type : "category",
				boundaryGap : false,
				data : mon
			}],
			yAxis : [ {
				type : "value",
				name : "%",
				nameLocation : "end"
			} ],
			series : [ {
				name : "soc",
				type : "line",
				data :batCap
			} ]
		};
		var myChart3 = echarts.init(document.getElementById('chongdian_cap'),
				"macarons");
		myChart3.setOption(option);

		option = {
			title : {
				text : "最高电压编号频次",
				x : "center"
			},
			tooltip : {
				trigger : "item",
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				x : "center",
				y : "bottom",
				data : legendData
			},
			toolbox : {
				show : true,
				feature : {
					dataView : {
						readOnly : true
					},
					saveAsImage : {
						show : true
					}
				}
			},
			calculable : true,
			series : [ {
				name : "面积模式",
				type : "pie",
				radius : [ 30, 90 ],
				center : [ "50%", "50%" ],
				roseType : "area",
				data : pieData
			} ]
		};
		var myChart4 = echarts.init(
				document.getElementById('chongdian_dianya'), "macarons");
		myChart4.setOption(option);

		option = {
			title : {
				text : "最高温度编号频次",
				x : "center"
			},
			tooltip : {
				trigger : "item",
				formatter : "{a} <br/>{b} : {c} ({d}%)"
			},
			legend : {
				x : "center",
				y : "bottom",
				data : legendDataTemp
			},
			toolbox : {
				show : true,
				feature : {
					dataView : {
						readOnly : true
					},
					saveAsImage : {
						show : true
					}
				}
			},
			calculable : true,
			series : [ {
				name : "面积模式",
				type : "pie",
				radius : [ 30, 90 ],
				center : [ "50%", "50%" ],
				roseType : "area",
				data : pieDataTemp
			} ]
		};
		var myChart5 = echarts.init(document.getElementById('chongdian_wendu'),
				"macarons");
		myChart5.setOption(option);

		setTimeout(function() {
			window.onresize = function() {
				myChart1.resize();
				myChart2.resize();
				myChart3.resize();
				myChart4.resize();
				myChart5.resize();
			}
		}, 200);