package com.diagnostic.action;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.diagnostic.service.ITCarWeekResultsService;


@Controller
@RequestMapping("car/report")
public class TCarWeekResultsAction {

	@Reference
	private ITCarWeekResultsService TCarWeekResultsService;

	@RequestMapping("/index/{sign}")
	public String index(@PathVariable String sign, Model m) {
		sign = "1G1BL52P7TR115520";

		m.addAttribute("status", "连线状态");
		m.addAttribute("soc", "'充电中<small>58.66%</small>'");
		m.addAttribute("temp_alert", "安全");
		m.addAttribute("estimation", TCarWeekResultsService.chrgBehaviorEstimation(sign));//充电行为综合评价

		m.addAttribute("mon", getMonths());
		JSONObject jsonCharg = TCarWeekResultsService.monChrg(sign);//月充电量/充电次数
		m.addAttribute("chrgCap", jsonCharg.getString("chrgCap"));
		m.addAttribute("chrgFery", jsonCharg.getString("chrgFery"));

		m.addAttribute("batCap", TCarWeekResultsService.batCapacity(sign));//月电池容量

		JSONObject jsonVolt = TCarWeekResultsService.voltFreyModulecell(sign);//最高电压频数
		m.addAttribute("legendData", jsonVolt.getString("legendData"));
		m.addAttribute("pieData", jsonVolt.getString("pieData"));

		JSONObject jsonTemp = TCarWeekResultsService.tempFreyNum(sign);//最高温度频数
		m.addAttribute("legendDataTemp", jsonTemp.getString("legendData"));
		m.addAttribute("pieDataTemp", jsonTemp.getString("pieData"));

		return "car/diagnostic-report";
	}


	/**
	 * @return 获取截止到当前月份的集合 形如 ['1月','2月']
	 */
	private List<String> getMonths() {
		List<String> month = new ArrayList<>();
		Calendar now = Calendar.getInstance();
		String currencyMonth = now.get(Calendar.MONTH) + 1 + "";
		for (int i = 1; i < Integer.parseInt(currencyMonth) + 1; i++) {
			month.add("'" + i + "月'");
		}
		return month;
	}
}
