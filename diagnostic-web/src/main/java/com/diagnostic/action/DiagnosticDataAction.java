package com.diagnostic.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.diagnostic.entity.TCarStartend;
import com.diagnostic.service.IDiagnosticDataService;
import com.diagnostic.utils.HttpResult;
import com.diagnostic.utils.IpAdrressUtil;

/**
 * 诊断数据接收(充电前后)
 */
@Controller
@RequestMapping("/accept")
public class DiagnosticDataAction {

	private static final Logger logger = LoggerFactory.getLogger(DiagnosticDataAction.class);

	@Reference
	private IDiagnosticDataService diagnosticDataService;

	/**
	 * 充电开始时的数据接收
	 * @param data
	 * @return
	 */
	@RequestMapping(value = "/chrg/start", method = RequestMethod.POST)
	@ResponseBody
	public HttpResult chrgStart(TCarStartend startPart, HttpServletRequest req) {
		try {
			HttpResult result = new HttpResult();
			logger.info("=> IP[" + IpAdrressUtil.getIpAdrress(req) + "]远程调用充电开始数据接收接口,参数:{}", JSONObject.toJSONString(startPart));

			// 车辆标识码
			if (StringUtils.isEmpty(startPart.getCarVh())) {
				result.setStatusCode(100);
				result.setMsg("车辆标识码[carVh]不能为空");
				return result;
			} else {
				if (startPart.getCarVh().length() > 20) {
					result.setStatusCode(101);
					result.setMsg("车辆标识码[carVh]长度不能超过20");
					return result;
				}
			}

			// 单体允许最高充电电压
			if (!StringUtils.isEmpty(startPart.getCellChrgMax())) {
				int maxvolt = startPart.getCellChrgMax().intValue();
				if (maxvolt < 0 || maxvolt > 10) {
					result.setStatusCode(102);
					result.setMsg("单体允许最高充电电压[cellChrgMax]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(103);
				result.setMsg("单体允许最高充电电压[cellChrgMax]不能为空");
				return result;
			}

			// 最高允许充电电流
			if (!StringUtils.isEmpty(startPart.getMaxLimitChrgCurr())) {
				int maxcurr = startPart.getMaxLimitChrgCurr().intValue();
				if (maxcurr < 0 || maxcurr > 1000) {
					result.setStatusCode(104);
					result.setMsg("最高允许充电电流[maxLimitChrgCurr]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(105);
				result.setMsg("最高允许充电电流[maxLimitChrgCurr]不能为空");
				return result;
			}

			// 动力蓄电池标称总能量
			if (!StringUtils.isEmpty(startPart.getBatNominaVolt())) {
				int batnominavolt = startPart.getBatNominaVolt().intValue();
				if (batnominavolt < 0 || batnominavolt > 1000) {
					result.setStatusCode(106);
					result.setMsg("动力蓄电池标称总能量[batNominaVolt]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(107);
				result.setMsg("动力蓄电池标称总能量[batNominaVolt]不能为空");
				return result;
			}

			// 最高允许充电总电压
			if (!StringUtils.isEmpty(startPart.getBatChrgMax())) {
				int maxvolt = startPart.getBatNominaVolt().intValue();
				if (maxvolt < 0 || maxvolt > 2000) {
					result.setStatusCode(108);
					result.setMsg("最高允许充电总电压[batChrgMax]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(109);
				result.setMsg("最高允许充电总电压[batChrgMax]不能为空");
				return result;
			}

			// 最高允许温度
			if (!StringUtils.isEmpty(startPart.getMaxLimitChrgTemp())) {
				int maxtemp = startPart.getMaxLimitChrgTemp().intValue();
				if (maxtemp < -50 || maxtemp > 200) {
					result.setStatusCode(110);
					result.setMsg("最高允许温度[maxLimitChrgTemp]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(111);
				result.setMsg("最高允许温度[maxLimitChrgTemp]不能为空");
				return result;
			}

			// 整车当前核电状态
			if (!StringUtils.isEmpty(startPart.getBatChrgSoc())) {
				int s1 = startPart.getBatChrgSoc().intValue();
				if (s1 < 0 || s1 > 100) {
					result.setStatusCode(112);
					result.setMsg("整车当前核电状态[batChrgSoc]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(113);
				result.setMsg("整车当前核电状态[batChrgSoc]不能为空");
				return result;
			}

			// 整车蓄电池当前电压
			if (!StringUtils.isEmpty(startPart.getBatChrgVolt())) {
				int volt = startPart.getBatChrgVolt().intValue();
				if (volt < 0 || volt >= 2000) {
					result.setStatusCode(114);
					result.setMsg("整车蓄电池当前电压[batChrgVolt]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(115);
				result.setMsg("整车蓄电池当前电压[batChrgVolt]不能为空");
				return result;
			}

			// 电池类型
			if (StringUtils.isEmpty(startPart.getBatType())) {
				result.setStatusCode(116);
				result.setMsg("电池类型[batType]不能为空");
				return result;
			} else {
				// 电池类型(1.铅酸，2.镍氢，3.磷酸铁锂，4.锰酸锂，5.钴酸锂，6.三元，7.聚合物锂离子，8.钛酸锂，255.其他电池)
				if (startPart.getBatType() != 1 && startPart.getBatType() != 2 && startPart.getBatType() != 3 && startPart.getBatType() != 4 && startPart.getBatType() != 5
						&& startPart.getBatType() != 6 && startPart.getBatType() != 7 && startPart.getBatType() != 8 && startPart.getBatType() != 255) {
					result.setStatusCode(117);
					result.setMsg("电池类型[batType]数据不合法");
					return result;
				}
			}

			// 电池包额定电压
			if (!StringUtils.isEmpty(startPart.getBatRtVol())) {
				int rtvolt = startPart.getBatRtVol().intValue();
				if (rtvolt < 0 || rtvolt > 2000) {
					result.setStatusCode(118);
					result.setMsg("电池包额定电压[batRtVol]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(119);
				result.setMsg("电池包额定电压[batRtVol]不能为空");
				return result;
			}

			// 电池包额定容量
			if (!StringUtils.isEmpty(startPart.getBatRtCap())) {
				int rtcap = startPart.getBatRtCap().intValue();
				if (rtcap < 0 || rtcap > 1000) {
					result.setStatusCode(120);
					result.setMsg("电池包额定容量[batRtCap]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(121);
				result.setMsg("电池包额定容量[batRtCap]不能为空");
				return result;
			}

			// 报文时间（充电前）
			if (!StringUtils.isEmpty(startPart.getDataTimeStart())) {
				if (startPart.getDataTimeStart() < 1500000000 || startPart.getDataTimeStart() > Integer.MAX_VALUE) {
					result.setStatusCode(122);
					result.setMsg("报文时间（充电前）[dataTimeStart]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(123);
				result.setMsg("报文时间（充电前）[dataTimeStart]不能为空");
				return result;
			}
			diagnosticDataService.chrgStart(startPart);
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail("充电开始数据接收接口异常，请联系诊断平台。");
		}
		return HttpResult.success("充电开始数据接收接口调用成功");
	}

	/**
	 * 充电结束时的数据接收
	 * @param data
	 * @return
	 */
	@RequestMapping(value = "/chrg/end", method = RequestMethod.POST)
	@ResponseBody
	public HttpResult chrgEnd(TCarStartend endPart, HttpServletRequest req) {
		try {
			HttpResult result = new HttpResult();
			logger.info("=> IP[" + IpAdrressUtil.getIpAdrress(req) + "]远程调用充电结束数据接收接口,参数:{}", JSONObject.toJSONString(endPart));

			// 车辆标识码
			if (StringUtils.isEmpty(endPart.getCarVh())) {
				result.setStatusCode(100);
				result.setMsg("车辆标识码[carVh]不能为空");
				return result;
			} else {
				if (endPart.getCarVh().length() > 20) {
					result.setStatusCode(101);
					result.setMsg("车辆标识码[carVh]长度不能超过20");
					return result;
				}
			}

			// 充电结束时最高单体电压
			if (!StringUtils.isEmpty(endPart.getCellVoltMaxEnd())) {
				int maxvolt = endPart.getCellVoltMaxEnd().intValue();
				if (maxvolt < 0 || maxvolt > 10) {
					result.setStatusCode(130);
					result.setMsg("充电结束时最高单体电压[cellVoltMaxEnd]数据不合法");
					return result;
				}
			} /*
				 * else { result.setStatusCode(131); result.setMsg("充电结束时最高单体电压[cellVoltMaxEnd]不能为空"); return result; }
				 */

			// 充电结束时最低单体电压
			if (!StringUtils.isEmpty(endPart.getCellVoltMinEnd())) {
				int minvolt = endPart.getCellVoltMinEnd().intValue();
				if (minvolt < 0 || minvolt > 10) {
					result.setStatusCode(132);
					result.setMsg("充电结束时最低单体电压[cellVoltMinEnd]数据不合法");
					return result;
				}
			} /*
				 * else { result.setStatusCode(133); result.setMsg("充电结束时最低单体电压[cellVoltMinEnd]不能为空"); return result; }
				 */

			// 充电结束时最低温度
			if (!StringUtils.isEmpty(endPart.getTempMinEnd())) {
				int mintemp = endPart.getTempMinEnd().intValue();
				if (mintemp < -50 || mintemp > 200) {
					result.setStatusCode(134);
					result.setMsg("充电结束时最低温度[tempMinEnd]数据不合法");
					return result;
				}
			} /*
				 * else { result.setStatusCode(135); result.setMsg("充电结束时最低温度[tempMinEnd]不能为空"); return result; }
				 */

			// 充电结束时最高温度
			if (!StringUtils.isEmpty(endPart.getTempMaxEnd())) {
				int maxtemp = endPart.getTempMaxEnd().intValue();
				if (maxtemp < -50 || maxtemp > 200) {
					result.setStatusCode(136);
					result.setMsg("充电结束时最高温度[tempMaxEnd]数据不合法");
					return result;
				}
			} /*
				 * else { result.setStatusCode(137); result.setMsg("充电结束时最高温度[tempMaxEnd]不能为空"); return result; }
				 */

			// 终止荷电状态SOC
			if (!StringUtils.isEmpty(endPart.getBatSocEnd())) {
				int soc = endPart.getBatSocEnd().intValue();
				if (soc < 0 || soc > 100) {
					result.setStatusCode(138);
					result.setMsg("终止荷电状态SOC[batSocEnd]数据不合法");
					return result;
				}
			} /*
				 * else { result.setStatusCode(139); result.setMsg("终止荷电状态SOC[batSocEnd]不能为空"); return result; }
				 */

			// 报文时间（充电后）
			if (!StringUtils.isEmpty(endPart.getDataTimeEnd())) {
				if (endPart.getDataTimeEnd() < 1500000000 || endPart.getDataTimeEnd() > Integer.MAX_VALUE) {
					result.setStatusCode(140);
					result.setMsg("报文时间（充电后）[dataTimeEnd]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(141);
				result.setMsg("报文时间（充电后）[dataTimeEnd]不能为空");
				return result;
			}
			diagnosticDataService.chrgEnd(endPart);
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail("充电结束数据接收接口异常，请联系诊断平台。");
		}
		return HttpResult.success("充电结束数据接收接口调用成功");
	}

}
