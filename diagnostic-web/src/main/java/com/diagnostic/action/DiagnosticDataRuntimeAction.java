package com.diagnostic.action;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.alibaba.fastjson.JSONObject;
import com.diagnostic.entity.TCarRuntime;
import com.diagnostic.service.IDiagnosticDataService;
import com.diagnostic.utils.HttpResult;
import com.diagnostic.utils.IpAdrressUtil;

/**
 * 诊断数据接收(充电中)
 */
@Controller
@RequestMapping("/accept")
public class DiagnosticDataRuntimeAction {

	private static final Logger logger = LoggerFactory.getLogger(DiagnosticDataRuntimeAction.class);

	@Reference
	private IDiagnosticDataService diagnosticDataService;

	/**
	 * 充电中的数据接收
	 * @param data
	 * @return
	 */
	@RequestMapping(value = "/chrg/runtime", method = RequestMethod.POST)
	@ResponseBody
	public HttpResult chrgRuntime(TCarRuntime runtimePart, HttpServletRequest req) {
		HttpResult result = new HttpResult();
		try {
			logger.info("=> IP[" + IpAdrressUtil.getIpAdrress(req) + "]远程调用充电中数据接收接口,参数:{}", JSONObject.toJSONString(runtimePart));
			// 车辆标识码
			if (StringUtils.isEmpty(runtimePart.getCarVh())) {
				result.setStatusCode(100);
				result.setMsg("车辆标识码[carVh]不能为空");
				return result;
			} else {
				if (runtimePart.getCarVh().length() > 20) {
					result.setStatusCode(101);
					result.setMsg("车辆标识码[carVh]长度不能超过20");
					return result;
				}
			}

			// 充电电压测量值
			if (!StringUtils.isEmpty(runtimePart.getBatVolt())) {
				int batvolt = runtimePart.getBatVolt().intValue();
				if (batvolt < 0 || batvolt > 2000) {
					result.setStatusCode(201);
					result.setMsg("充电电压测量值[batVolt]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(202);
				result.setMsg("充电电压测量值[batVolt]不能为空");
				return result;
			}

			// 充电电流测量值
			if (!StringUtils.isEmpty(runtimePart.getBatCurr())) {
				int batcurr = runtimePart.getBatCurr().intValue();
				if (batcurr < 0 || batcurr > 1000) {
					result.setStatusCode(203);
					result.setMsg("充电电流测量值[batCurr]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(204);
				result.setMsg("充电电流测量值[batCurr]不能为空");
				return result;
			}

			// 最高单体电压
			if (!StringUtils.isEmpty(runtimePart.getCellVoltMax())) {
				int maxvolt = runtimePart.getCellVoltMax().intValue();
				if (maxvolt < 0 || maxvolt > 2000) {
					result.setStatusCode(205);
					result.setMsg("最高单体电压[cellVoltMax]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(206);
				result.setMsg("最高单体电压[cellVoltMax]不能为空");
				return result;
			}

			// 最高单体电压组号/箱号
			if (!StringUtils.isEmpty(runtimePart.getModuleVoltNoMax())) {
				int maxvoltnum = runtimePart.getModuleVoltNoMax().intValue();
				if (maxvoltnum < 0 || maxvoltnum > 100) {
					result.setStatusCode(207);
					result.setMsg("最高单体电压组号/箱号[moduleVoltNoMax]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(208);
				result.setMsg("最高单体电压组号/箱号[moduleVoltNoMax]不能为空");
				return result;
			}

			// SOC
			if (!StringUtils.isEmpty(runtimePart.getBatSoc())) {
				int soc = runtimePart.getBatSoc().intValue();
				if (soc < 0 || soc > 100) {
					result.setStatusCode(209);
					result.setMsg("SOC[batSoc]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(210);
				result.setMsg("SOC[batSoc]不能为空");
				return result;
			}

			// 估计剩余充电时间
			if (!StringUtils.isEmpty(runtimePart.getChrgRemianTime())) {
				int time = runtimePart.getChrgRemianTime().intValue();
				if (time < 0 || time > 600) {
					result.setStatusCode(211);
					result.setMsg("估计剩余充电时间[chrgRemianTime] 数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(212);
				result.setMsg("估计剩余充电时间[chrgRemianTime]不能为空");
				return result;
			}

			// 最高单体电压编号
			if (!StringUtils.isEmpty(runtimePart.getCellVoltNoMax())) {
				int maxvoltnum = runtimePart.getCellVoltNoMax().intValue();
				if (maxvoltnum < 0 || maxvoltnum >= 1000) {
					result.setStatusCode(213);
					result.setMsg("最高单体电压编号[cellVoltNoMax] 数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(214);
				result.setMsg("最高单体电压编号[cellVoltNoMax] 不能为空");
				return result;
			}


			// 最高测点温度
			if (!StringUtils.isEmpty(runtimePart.getTempMax())) {
				int maxtemp = runtimePart.getTempMax().intValue();
				if (maxtemp < -50 || maxtemp > 200) {
					result.setStatusCode(215);
					result.setMsg("最高测点温度[tempMax] 数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(216);
				result.setMsg("最高测点温度[tempMax]不能为空");
				return result;
			}

			// 最高测点温度编号
			if (!StringUtils.isEmpty(runtimePart.getTempNoMax())) {
				int maxtempnum = runtimePart.getTempNoMax().intValue();
				if (maxtempnum < 1 || maxtempnum > 256) {
					result.setStatusCode(217);
					result.setMsg("最高测点温度编号[tempNoMax] 数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(218);
				result.setMsg("最高测点温度编号[tempNoMax] 不能为空");
				return result;
			}

			// 最低测点温度
			if (!StringUtils.isEmpty(runtimePart.getTempMin())) {
				int mintemp = runtimePart.getTempMin().intValue();
				if (mintemp < -50 || mintemp > 200) {
					result.setStatusCode(219);
					result.setMsg("最低测点温度[tempMin]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(220);
				result.setMsg("最低测点温度[tempMin]不能为空");
				return result;
			}

			// 最低测点温度编号
			if (!StringUtils.isEmpty(runtimePart.getTempNoMin())) {
				int mintempnum = runtimePart.getTempNoMin().intValue();
				if (mintempnum < 1 || mintempnum > 256) {
					result.setStatusCode(221);
					result.setMsg("最低测点温度编号[tempNoMin] 数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(222);
				result.setMsg("最低测点温度编号[tempNoMin]不能为空");
				return result;
			}
			
			// 电表读数
			if (!StringUtils.isEmpty(runtimePart.getAmmeterNum())) {
				int mintempnum = runtimePart.getAmmeterNum().intValue();
				if (mintempnum < 0 ) {
					result.setStatusCode(223);
					result.setMsg("电表读数[ammeterNum] 数据不合法");
					return result;
				}
			}
			
			// 报文时间（充电中）
			if (!StringUtils.isEmpty(runtimePart.getDataTime())) {
				if (runtimePart.getDataTime() < 1500000000 || runtimePart.getDataTime() > Integer.MAX_VALUE) {
					result.setStatusCode(223);
					result.setMsg("报文时间（充电中）[dataTime]数据不合法");
					return result;
				}
			} else {
				result.setStatusCode(224);
				result.setMsg("报文时间（充电中）[dataTime]不能为空");
				return result;
			}
			
			diagnosticDataService.chrgRuntime(runtimePart);
		} catch (Exception e) {
			logger.error(e.getMessage(), e.fillInStackTrace());
			return HttpResult.fail("充电中数据接收接口异常，请联系诊断平台。");
		}
		return HttpResult.success("充电中数据接收接口调用成功");
	}

}

