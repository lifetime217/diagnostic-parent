package com.diagnostic.action;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.dubbo.config.annotation.Reference;
import com.diagnostic.service.ITestService;

/**
 * @author lifetime
 *
 */
@Controller
@RequestMapping("test")
public class TestAction {

	@Reference
	private ITestService test;

	@GetMapping("t1/{key}")
	@ResponseBody
	public String t1(@PathVariable String key) {
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		System.out.println(2121);
		return test.test(key);
	}

}
