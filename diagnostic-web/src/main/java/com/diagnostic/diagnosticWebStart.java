package com.diagnostic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.alibaba.dubbo.config.spring.context.annotation.DubboComponentScan;

/**
 * @author lifetime
 *
 */
@SpringBootApplication
@DubboComponentScan(basePackages = "com.diagnostic.action")
public class diagnosticWebStart {

	public static void main(String[] args) {
		ConfigurableApplicationContext ctx = SpringApplication.run(diagnosticWebStart.class, args);
		String[] activeProfiles = ctx.getEnvironment().getActiveProfiles();
		for (String profile : activeProfiles) {
			System.out.println("========【diagnostic - Web】========= Spring Boot 使用profile为:" + profile);
		}
	}
}
