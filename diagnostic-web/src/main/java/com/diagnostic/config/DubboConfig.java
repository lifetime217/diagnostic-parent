package com.diagnostic.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

import com.alibaba.dubbo.config.ApplicationConfig;
import com.alibaba.dubbo.config.ConsumerConfig;
import com.alibaba.dubbo.config.RegistryConfig;

/**
 * @author lifetime
 *
 */
@Configuration
public class DubboConfig {

	@Autowired
	private Environment env;
	
    @Bean
    public ApplicationConfig applicationConfig() {
        ApplicationConfig applicationConfig = new ApplicationConfig();
        applicationConfig.setName("diagnostic-web");
        applicationConfig.setQosEnable(false);
        return applicationConfig;
    }

    @Bean
    public ConsumerConfig consumerConfig() {
        ConsumerConfig consumerConfig = new ConsumerConfig();
        consumerConfig.setTimeout(env.getProperty("dubbo.consumer.timeout", Integer.class, 10000));
        consumerConfig.setRetries(env.getProperty("dubbo.consumer.retries", Integer.class, 1));
        return consumerConfig;
    }

    @Bean
    public RegistryConfig registryConfig() {
        RegistryConfig registryConfig = new RegistryConfig();
        registryConfig.setAddress(env.getProperty("dubbo.registry.address"));
        registryConfig.setClient("curator");
        return registryConfig;
    }

}
