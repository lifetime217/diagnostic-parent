package com.diagnostic.config;

import java.nio.charset.Charset;
import java.util.Arrays;

import javax.servlet.Filter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter4;

/**
 * @author lifetime
 *
 */
@Configuration
public class WebConfig {

	@Configuration
	@ConditionalOnClass({ FastJsonHttpMessageConverter4.class })
	@ConditionalOnProperty(name = {
			"spring.http.converters.preferred-json-mapper" }, havingValue = "fastjson", matchIfMissing = true)
	protected static class FastJson2HttpMessageConverterConfiguration {

		protected FastJson2HttpMessageConverterConfiguration() {
		}

		@Bean
		@ConditionalOnMissingBean({ FastJsonHttpMessageConverter4.class })
		public FastJsonHttpMessageConverter4 fastJsonHttpMessageConverter() {
			FastJsonHttpMessageConverter4 converter = new FastJsonHttpMessageConverter4();
			FastJsonConfig fastJsonConfig = new FastJsonConfig();
			fastJsonConfig.setCharset(Charset.forName("UTF-8"));
			fastJsonConfig.setSerializerFeatures(SerializerFeature.PrettyFormat, SerializerFeature.WriteMapNullValue,
					SerializerFeature.WriteDateUseDateFormat);

			converter.setFastJsonConfig(fastJsonConfig);

			converter.setSupportedMediaTypes(Arrays.asList(new MediaType[] { MediaType.APPLICATION_JSON }));
			return converter;
		}
	}

	@Bean
	public HandlerExceptionResolver customerExceptionPage() {
		HandlerExceptionResolver exceptionResolver = new HandlerExceptionResolver() {
			public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response,
					Object handler, Exception ex) {
				ModelAndView mav = new ModelAndView("/error-500");
				mav.addObject("errorMsg", ex.getMessage());
				return mav;
			}
		};
		return exceptionResolver;
	}

	@Bean
	public Filter characterEncodingFilter() {
		CharacterEncodingFilter characterEncodingFilter = new CharacterEncodingFilter();
		characterEncodingFilter.setEncoding("UTF-8");
		characterEncodingFilter.setForceEncoding(true);
		return characterEncodingFilter;
	}


}
