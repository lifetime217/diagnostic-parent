package com.diagnostic.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import com.diagnostic.BaseInfo;

/**
 * 车辆（开始/结束）充电数据表
 */
public class TCarStartend extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public TCarStartend() {
	}

	/**
	 * 
	 */
	public Integer getId() {
		return getInteger("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * runtime中对应充电过程终止seq值
	 */
	public Integer getRuntimeSeq() {
		return getInteger("runtimeSeq");
	}

	public void setRuntimeSeq(Integer runtimeSeq) {
		set("runtimeSeq", runtimeSeq);
	}

	/**
	 * 车辆标识符
	 */
	public String getCarVh() {
		return getString("carVh");
	}

	public void setCarVh(String carVh) {
		set("carVh", carVh);
	}

	/**
	 * 单体允许最高充电电压
	 */
	public BigDecimal getCellChrgMax() {
		return getBigDecimal("cellChrgMax");
	}

	public void setCellChrgMax(BigDecimal cellChrgMax) {
		set("cellChrgMax", cellChrgMax);
	}

	/**
	 * 最高允许充电电流
	 */
	public BigDecimal getMaxLimitChrgCurr() {
		return getBigDecimal("maxLimitChrgCurr");
	}

	public void setMaxLimitChrgCurr(BigDecimal maxLimitChrgCurr) {
		set("maxLimitChrgCurr", maxLimitChrgCurr);
	}

	/**
	 * 动力蓄电池标称总能量
	 */
	public BigDecimal getBatNominaVolt() {
		return getBigDecimal("batNominaVolt");
	}

	public void setBatNominaVolt(BigDecimal batNominaVolt) {
		set("batNominaVolt", batNominaVolt);
	}

	/**
	 * 最高允许充电总电压
	 */
	public BigDecimal getBatChrgMax() {
		return getBigDecimal("batChrgMax");
	}

	public void setBatChrgMax(BigDecimal batChrgMax) {
		set("batChrgMax", batChrgMax);
	}

	/**
	 * 最高允许温度
	 */
	public BigDecimal getMaxLimitChrgTemp() {
		return getBigDecimal("maxLimitChrgTemp");
	}

	public void setMaxLimitChrgTemp(BigDecimal maxLimitChrgTemp) {
		set("maxLimitChrgTemp", maxLimitChrgTemp);
	}

	/**
	 * 整车当前核电状态（充电前）
	 */
	public BigDecimal getBatChrgSoc() {
		return getBigDecimal("batChrgSoc");
	}

	public void setBatChrgSoc(BigDecimal batChrgSoc) {
		set("batChrgSoc", batChrgSoc);
	}

	/**
	 * 整车蓄电池当前电压（充电前）
	 */
	public BigDecimal getBatChrgVolt() {
		return getBigDecimal("batChrgVolt");
	}

	public void setBatChrgVolt(BigDecimal batChrgVolt) {
		set("batChrgVolt", batChrgVolt);
	}

	/**
	 * 电池类型(1.铅酸，2.镍氢，3.磷酸铁锂，4.锰酸锂，5.钴酸锂，6.三元，7.聚合物锂离子，8.钛酸锂，255.其他电池)
	 */
	public Integer getBatType() {
		return getInteger("batType");
	}

	public void setBatType(Integer batType) {
		set("batType", batType);
	}

	/**
	 * 电池包额定电压
	 */
	public BigDecimal getBatRtVol() {
		return getBigDecimal("batRtVol");
	}

	public void setBatRtVol(BigDecimal batRtVol) {
		set("batRtVol", batRtVol);
	}

	/**
	 * 电池包额定容量
	 */
	public BigDecimal getBatRtCap() {
		return getBigDecimal("batRtCap");
	}

	public void setBatRtCap(BigDecimal batRtCap) {
		set("batRtCap", batRtCap);
	}

	/**
	 * 充电结束时最高单体电压
	 */
	public BigDecimal getCellVoltMaxEnd() {
		return getBigDecimal("cellVoltMaxEnd");
	}

	public void setCellVoltMaxEnd(BigDecimal cellVoltMaxEnd) {
		set("cellVoltMaxEnd", cellVoltMaxEnd);
	}

	/**
	 * 充电结束时最低单体电压
	 */
	public BigDecimal getCellVoltMinEnd() {
		return getBigDecimal("cellVoltMinEnd");
	}

	public void setCellVoltMinEnd(BigDecimal cellVoltMinEnd) {
		set("cellVoltMinEnd", cellVoltMinEnd);
	}

	/**
	 * 中止荷电状态SOC
	 */
	public BigDecimal getBatSocEnd() {
		return getBigDecimal("batSocEnd");
	}

	public void setBatSocEnd(BigDecimal batSocEnd) {
		set("batSocEnd", batSocEnd);
	}

	/**
	 * 充电结束时最低温度
	 */
	public BigDecimal getTempMinEnd() {
		return getBigDecimal("tempMinEnd");
	}

	public void setTempMinEnd(BigDecimal tempMinEnd) {
		set("tempMinEnd", tempMinEnd);
	}

	/**
	 * 充电结束时最高温度
	 */
	public BigDecimal getTempMaxEnd() {
		return getBigDecimal("tempMaxEnd");
	}

	public void setTempMaxEnd(BigDecimal tempMaxEnd) {
		set("tempMaxEnd", tempMaxEnd);
	}

	/**
	 * 报文时间（充电前）
	 */
	public Integer getDataTimeStart() {
		return getInteger("dataTimeStart");
	}

	public void setDataTimeStart(Integer dataTimeStart) {
		set("dataTimeStart", dataTimeStart);
	}

	/**
	 * 入库时间（充电前）
	 */
	public Integer getAddTimeStart() {
		return getInteger("addTimeStart");
	}

	public void setAddTimeStart(Integer addTimeStart) {
		set("addTimeStart", addTimeStart);
	}

	/**
	 * 报文时间（充电后）
	 */
	public Integer getDataTimeEnd() {
		return getInteger("dataTimeEnd");
	}

	public void setDataTimeEnd(Integer dataTimeEnd) {
		set("dataTimeEnd", dataTimeEnd);
	}

	/**
	 * 入库时间（充电后）
	 */
	public Integer getAddTimeEnd() {
		return getInteger("addTimeEnd");
	}

	public void setAddTimeEnd(Integer addTimeEnd) {
		set("addTimeEnd", addTimeEnd);
	}

	/**
	 * 公司简称
	 */
	public String getCompanyShortName() {
		return getString("companyShortName");
	}

	public void setCompanyShortName(String companyShortName) {
		set("companyShortName", companyShortName);
	}

}