package com.diagnostic.entity;

import java.io.Serializable;

import org.beetl.sql.core.annotatoin.AssignID;

import com.diagnostic.BaseInfo;

/**
 * 车辆的信息表
 */
public class TCarInfo extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public TCarInfo() {
	}
	
	/**
	 * 
	 */
	@AssignID("uuid")
	public String getId() {
		return getString("id");
	}

	public void setId(String id) {
		set("id", id);
	}


	/**
	 * 
	 */
	public String getCarVh() {
		return getString("carVh");
	}

	public void setCarVh(String carVh) {
		set("carVh", carVh);
	}

	/**
	 * 
	 */
	public Integer getUpdateTime() {
		return getInteger("updateTime");
	}

	public void setUpdateTime(Integer updateTime) {
		set("updateTime", updateTime);
	}

	/**
	 * 温度的结果表示
	 */
	public String getChrgTempState() {
		return getString("chrgTempState");
	}

	public void setChrgTempState(String chrgTempState) {
		set("chrgTempState", chrgTempState);
	}

}