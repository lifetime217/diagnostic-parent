package com.diagnostic.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import com.diagnostic.BaseInfo;

/**
 * 车辆接受实时数据表（充电中）
 */
public class TCarRuntime extends BaseInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	public TCarRuntime() {
	}

	/**
	 * 
	 */
	public Integer getId() {
		return getInteger("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * 
	 */
	public Integer getCarStartendId() {
		return getInteger("carStartendId");
	}

	public void setCarStartendId(Integer carStartendId) {
		set("carStartendId", carStartendId);
	}

	/**
	 * 车辆标识符（冗余）
	 */
	public String getCarVh() {
		return getString("carVh");
	}

	public void setCarVh(String carVh) {
		set("carVh", carVh);
	}

	/**
	 * 序号
	 */
	public Integer getSeq() {
		return getInteger("seq");
	}

	public void setSeq(Integer seq) {
		set("seq", seq);
	}

	/**
	 * 信息时间（年月日）
	 */
	public Integer getInfTimeDay() {
		return getInteger("infTimeDay");
	}

	public void setInfTimeDay(Integer infTimeDay) {
		set("infTimeDay", infTimeDay);
	}

	/**
	 * 信息时间（时分秒）
	 */
	public Integer getInfTimeSecond() {
		return getInteger("infTimeSecond");
	}

	public void setInfTimeSecond(Integer infTimeSecond) {
		set("infTimeSecond", infTimeSecond);
	}

	/**
	 * 充电电压测量值
	 */
	public BigDecimal getBatVolt() {
		return getBigDecimal("batVolt");
	}

	public void setBatVolt(BigDecimal batVolt) {
		set("batVolt", batVolt);
	}

	/**
	 * 充电电流测量值
	 */
	public BigDecimal getBatCurr() {
		return getBigDecimal("batCurr");
	}

	public void setBatCurr(BigDecimal batCurr) {
		set("batCurr", batCurr);
	}

	/**
	 * 最高单体电压
	 */
	public BigDecimal getCellVoltMax() {
		return getBigDecimal("cellVoltMax");
	}

	public void setCellVoltMax(BigDecimal cellVoltMax) {
		set("cellVoltMax", cellVoltMax);
	}

	/**
	 * 最高单体电压组号/箱号
	 */
	public Integer getModuleVoltNoMax() {
		return getInteger("moduleVoltNoMax");
	}

	public void setModuleVoltNoMax(Integer moduleVoltNoMax) {
		set("moduleVoltNoMax", moduleVoltNoMax);
	}

	/**
	 * SOC
	 */
	public BigDecimal getBatSoc() {
		return getBigDecimal("batSoc");
	}

	public void setBatSoc(BigDecimal batSoc) {
		set("batSoc", batSoc);
	}

	/**
	 * 估计剩余充电时间
	 */
	public BigDecimal getChrgRemianTime() {
		return getBigDecimal("chrgRemianTime");
	}

	public void setChrgRemianTime(BigDecimal chrgRemianTime) {
		set("chrgRemianTime", chrgRemianTime);
	}

	/**
	 * 最高单体电压编号
	 */
	public Integer getCellVoltNoMax() {
		return getInteger("cellVoltNoMax");
	}

	public void setCellVoltNoMax(Integer cellVoltNoMax) {
		set("cellVoltNoMax", cellVoltNoMax);
	}

	/**
	 * 最高测点温度
	 */
	public BigDecimal getTempMax() {
		return getBigDecimal("tempMax");
	}

	public void setTempMax(BigDecimal tempMax) {
		set("tempMax", tempMax);
	}

	/**
	 * 最高测点温度编号
	 */
	public Integer getTempNoMax() {
		return getInteger("tempNoMax");
	}

	public void setTempNoMax(Integer tempNoMax) {
		set("tempNoMax", tempNoMax);
	}

	/**
	 * 最低测点温度
	 */
	public BigDecimal getTempMin() {
		return getBigDecimal("tempMin");
	}

	public void setTempMin(BigDecimal tempMin) {
		set("tempMin", tempMin);
	}

	/**
	 * 最低测点温度编号
	 */
	public Integer getTempNoMin() {
		return getInteger("tempNoMin");
	}

	public void setTempNoMin(Integer tempNoMin) {
		set("tempNoMin", tempNoMin);
	}

	/**
	 * 本次充电容量
	 */
	public BigDecimal getDcChrgCap() {
		return getBigDecimal("dcChrgCap");
	}

	public void setDcChrgCap(BigDecimal dcChrgCap) {
		set("dcChrgCap", dcChrgCap);
	}

	/**
	 * 电表读数
	 */
	public BigDecimal getAmmeterNum() {
		return getBigDecimal("ammeterNum");
	}

	public void setAmmeterNum(BigDecimal ammeterNum) {
		set("ammeterNum", ammeterNum);
	}

	/**
	 * 温度极差
	 */
	public BigDecimal getTempMaxMin() {
		return getBigDecimal("tempMaxMin");
	}

	public void setTempMaxMin(BigDecimal tempMaxMin) {
		set("tempMaxMin", tempMaxMin);
	}

	/**
	 * 平均温度
	 */
	public BigDecimal getTempAvg() {
		return getBigDecimal("tempAvg");
	}

	public void setTempAvg(BigDecimal tempAvg) {
		set("tempAvg", tempAvg);
	}

	/**
	 * 报文时间
	 */
	public Integer getDataTime() {
		return getInteger("dataTime");
	}

	public void setDataTime(Integer dataTime) {
		set("dataTime", dataTime);
	}

	/**
	 * 入库时间
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 公司简称
	 */
	public String getCompanyShortName() {
		return getString("companyShortName");
	}

	public void setCompanyShortName(String companyShortName) {
		set("companyShortName", companyShortName);
	}
	
	

}