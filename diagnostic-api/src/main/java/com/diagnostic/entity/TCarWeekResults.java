package com.diagnostic.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import com.diagnostic.BaseInfo;

public class TCarWeekResults extends BaseInfo implements Serializable{
	private static final long serialVersionUID = 1L;

	public TCarWeekResults() {
	}
	/**
	 * 
	 */
	public Integer getId() {
		return getInteger("id");
	}

	public void setId(Integer id) {
		set("id", id);
	}

	/**
	 * 公司简称
	 */
	public String getCompanyShortName() {
		return getString("companyShortName");
	}

	public void setCompanyShortName(String companyShortName) {
		set("companyShortName", companyShortName);
	}

	/**
	 * 车辆标识码
	 */
	public String getCarVh() {
		return getString("carVh");
	}

	public void setCarVh(String carVh) {
		set("carVh", carVh);
	}

	/**
	 * 结果写入时间
	 */
	public Integer getAddTime() {
		return getInteger("addTime");
	}

	public void setAddTime(Integer addTime) {
		set("addTime", addTime);
	}

	/**
	 * 
	 */
	public Integer getWeek() {
		return getInteger("week");
	}

	public void setWeek(Integer week) {
		set("week", week);
	}

	/**
	 * 快充频率
	 */
	public Integer getChrgDcFery() {
		return getInteger("chrgDcFery");
	}

	public void setChrgDcFery(Integer chrgDcFery) {
		set("chrgDcFery", chrgDcFery);
	}

	/**
	 * 快充频率得分
	 */
	public BigDecimal getChrgDcFeryScore() {
		return getBigDecimal("chrgDcFeryScore");
	}

	public void setChrgDcFeryScore(BigDecimal chrgDcFeryScore) {
		set("chrgDcFeryScore", chrgDcFeryScore);
	}

	/**
	 * 过放电频率
	 */
	public BigDecimal getDschOverFery() {
		return getBigDecimal("dschOverFery");
	}

	public void setDschOverFery(BigDecimal dschOverFery) {
		set("dschOverFery", dschOverFery);
	}

	/**
	 * 过放电频率得分
	 */
	public BigDecimal getDschOverFeryScore() {
		return getBigDecimal("dschOverFeryScore");
	}

	public void setDschOverFeryScore(BigDecimal dschOverFeryScore) {
		set("dschOverFeryScore", dschOverFeryScore);
	}

	/**
	 * 平均放电深度
	 */
	public BigDecimal getDschDodAvg() {
		return getBigDecimal("dschDodAvg");
	}

	public void setDschDodAvg(BigDecimal dschDodAvg) {
		set("dschDodAvg", dschDodAvg);
	}

	/**
	 * 充放电深度得分
	 */
	public BigDecimal getDschDodScore() {
		return getBigDecimal("dschDodScore");
	}

	public void setDschDodScore(BigDecimal dschDodScore) {
		set("dschDodScore", dschDodScore);
	}

	/**
	 * 峰电价充电占比
	 */
	public BigDecimal getChrgPeakTimeRate() {
		return getBigDecimal("chrgPeakTimeRate");
	}

	public void setChrgPeakTimeRate(BigDecimal chrgPeakTimeRate) {
		set("chrgPeakTimeRate", chrgPeakTimeRate);
	}

	/**
	 * 平电价充电占比
	 */
	public BigDecimal getChrgNounTimeRate() {
		return getBigDecimal("chrgNounTimeRate");
	}

	public void setChrgNounTimeRate(BigDecimal chrgNounTimeRate) {
		set("chrgNounTimeRate", chrgNounTimeRate);
	}

	/**
	 * 谷电价充电占比
	 */
	public BigDecimal getChrgVallyTimeRate() {
		return getBigDecimal("chrgVallyTimeRate");
	}

	public void setChrgVallyTimeRate(BigDecimal chrgVallyTimeRate) {
		set("chrgVallyTimeRate", chrgVallyTimeRate);
	}

	/**
	 * 充电时段得分
	 */
	public BigDecimal getChrgTimeScore() {
		return getBigDecimal("chrgTimeScore");
	}

	public void setChrgTimeScore(BigDecimal chrgTimeScore) {
		set("chrgTimeScore", chrgTimeScore);
	}

	/**
	 * 正常结束充电频率
	 */
	public BigDecimal getChrgEndFery() {
		return getBigDecimal("chrgEndFery");
	}

	public void setChrgEndFery(BigDecimal chrgEndFery) {
		set("chrgEndFery", chrgEndFery);
	}

	/**
	 * 正常结束充电频率得分
	 */
	public BigDecimal getChrgEndFeryScore() {
		return getBigDecimal("chrgEndFeryScore");
	}

	public void setChrgEndFeryScore(BigDecimal chrgEndFeryScore) {
		set("chrgEndFeryScore", chrgEndFeryScore);
	}

	/**
	 * 充电综合评分
	 */
	public BigDecimal getChrgScore() {
		return getBigDecimal("chrgScore");
	}

	public void setChrgScore(BigDecimal chrgScore) {
		set("chrgScore", chrgScore);
	}

	/**
	 * 最高单体序号频率第1的电池组号/箱号
	 */
	public String getModuleVoltMaxNo1() {
		return getString("moduleVoltMaxNo1");
	}

	public void setModuleVoltMaxNo1(String moduleVoltMaxNo1) {
		set("moduleVoltMaxNo1", moduleVoltMaxNo1);
	}

	/**
	 * 最高单体序号频率第2的电池组号/箱号
	 */
	public String getModuleVoltMaxNo2() {
		return getString("moduleVoltMaxNo2");
	}

	public void setModuleVoltMaxNo2(String moduleVoltMaxNo2) {
		set("moduleVoltMaxNo2", moduleVoltMaxNo2);
	}

	/**
	 * 最高单体序号频率第3的电池组号/箱号
	 */
	public String getModuleVoltMaxNo3() {
		return getString("moduleVoltMaxNo3");
	}

	public void setModuleVoltMaxNo3(String moduleVoltMaxNo3) {
		set("moduleVoltMaxNo3", moduleVoltMaxNo3);
	}

	/**
	 * 最高单体序号频率第4的电池组号/箱号
	 */
	public String getModuleVoltMaxNo4() {
		return getString("moduleVoltMaxNo4");
	}

	public void setModuleVoltMaxNo4(String moduleVoltMaxNo4) {
		set("moduleVoltMaxNo4", moduleVoltMaxNo4);
	}

	/**
	 * 最高单体序号频率第5的电池组号/箱号
	 */
	public String getModuleVoltMaxNo5() {
		return getString("moduleVoltMaxNo5");
	}

	public void setModuleVoltMaxNo5(String moduleVoltMaxNo5) {
		set("moduleVoltMaxNo5", moduleVoltMaxNo5);
	}

	/**
	 * 最高单体序号频率第6的电池组号/箱号
	 */
	public String getModuleVoltMaxNo6() {
		return getString("moduleVoltMaxNo6");
	}

	public void setModuleVoltMaxNo6(String moduleVoltMaxNo6) {
		set("moduleVoltMaxNo6", moduleVoltMaxNo6);
	}

	/**
	 * 最高单体序号频率第7的电池组号/箱号
	 */
	public String getModuleVoltMaxNo7() {
		return getString("moduleVoltMaxNo7");
	}

	public void setModuleVoltMaxNo7(String moduleVoltMaxNo7) {
		set("moduleVoltMaxNo7", moduleVoltMaxNo7);
	}

	/**
	 * 最高单体序号频率第8的电池组号/箱号
	 */
	public String getModuleVoltMaxNo8() {
		return getString("moduleVoltMaxNo8");
	}

	public void setModuleVoltMaxNo8(String moduleVoltMaxNo8) {
		set("moduleVoltMaxNo8", moduleVoltMaxNo8);
	}

	/**
	 * 最高单体序号频率第9的电池组号/箱号
	 */
	public String getModuleVoltMaxNo9() {
		return getString("moduleVoltMaxNo9");
	}

	public void setModuleVoltMaxNo9(String moduleVoltMaxNo9) {
		set("moduleVoltMaxNo9", moduleVoltMaxNo9);
	}

	/**
	 * 最高单体序号频率第10的电池组号/箱号
	 */
	public String getModuleVoltMaxNo10() {
		return getString("moduleVoltMaxNo10");
	}

	public void setModuleVoltMaxNo10(String moduleVoltMaxNo10) {
		set("moduleVoltMaxNo10", moduleVoltMaxNo10);
	}

	/**
	 * 最高单体序号频率第1的编号
	 */
	public String getCellVoltMaxNo1() {
		return getString("cellVoltMaxNo1");
	}

	public void setCellVoltMaxNo1(String cellVoltMaxNo1) {
		set("cellVoltMaxNo1", cellVoltMaxNo1);
	}

	/**
	 * 最高单体序号频率第2的编号
	 */
	public String getCellVoltMaxNo2() {
		return getString("cellVoltMaxNo2");
	}

	public void setCellVoltMaxNo2(String cellVoltMaxNo2) {
		set("cellVoltMaxNo2", cellVoltMaxNo2);
	}

	/**
	 * 最高单体序号频率第3的编号
	 */
	public String getCellVoltMaxNo3() {
		return getString("cellVoltMaxNo3");
	}

	public void setCellVoltMaxNo3(String cellVoltMaxNo3) {
		set("cellVoltMaxNo3", cellVoltMaxNo3);
	}

	/**
	 * 最高单体序号频率第4的编号
	 */
	public String getCellVoltMaxNo4() {
		return getString("cellVoltMaxNo4");
	}

	public void setCellVoltMaxNo4(String cellVoltMaxNo4) {
		set("cellVoltMaxNo4", cellVoltMaxNo4);
	}

	/**
	 * 最高单体序号频率第5的编号
	 */
	public String getCellVoltMaxNo5() {
		return getString("cellVoltMaxNo5");
	}

	public void setCellVoltMaxNo5(String cellVoltMaxNo5) {
		set("cellVoltMaxNo5", cellVoltMaxNo5);
	}

	/**
	 * 最高单体序号频率第6的编号
	 */
	public String getCellVoltMaxNo6() {
		return getString("cellVoltMaxNo6");
	}

	public void setCellVoltMaxNo6(String cellVoltMaxNo6) {
		set("cellVoltMaxNo6", cellVoltMaxNo6);
	}

	/**
	 * 最高单体序号频率第7的编号
	 */
	public String getCellVoltMaxNo7() {
		return getString("cellVoltMaxNo7");
	}

	public void setCellVoltMaxNo7(String cellVoltMaxNo7) {
		set("cellVoltMaxNo7", cellVoltMaxNo7);
	}

	/**
	 * 最高单体序号频率第8的编号
	 */
	public String getCellVoltMaxNo8() {
		return getString("cellVoltMaxNo8");
	}

	public void setCellVoltMaxNo8(String cellVoltMaxNo8) {
		set("cellVoltMaxNo8", cellVoltMaxNo8);
	}

	/**
	 * 最高单体序号频率第9的编号
	 */
	public String getCellVoltMaxNo9() {
		return getString("cellVoltMaxNo9");
	}

	public void setCellVoltMaxNo9(String cellVoltMaxNo9) {
		set("cellVoltMaxNo9", cellVoltMaxNo9);
	}

	/**
	 * 最高单体序号频率第10的编号
	 */
	public String getCellVoltMaxNo10() {
		return getString("cellVoltMaxNo10");
	}

	public void setCellVoltMaxNo10(String cellVoltMaxNo10) {
		set("cellVoltMaxNo10", cellVoltMaxNo10);
	}

	/**
	 * 最高单体序号频率第1的频率
	 */
	public BigDecimal getVoltMaxFreyNo1() {
		return getBigDecimal("voltMaxFreyNo1");
	}

	public void setVoltMaxFreyNo1(BigDecimal voltMaxFreyNo1) {
		set("voltMaxFreyNo1", voltMaxFreyNo1);
	}

	/**
	 * 最高单体序号频率第2的频率
	 */
	public BigDecimal getVoltMaxFreyNo2() {
		return getBigDecimal("voltMaxFreyNo2");
	}

	public void setVoltMaxFreyNo2(BigDecimal voltMaxFreyNo2) {
		set("voltMaxFreyNo2", voltMaxFreyNo2);
	}

	/**
	 * 最高单体序号频率第3的频率
	 */
	public BigDecimal getVoltMaxFreyNo3() {
		return getBigDecimal("voltMaxFreyNo3");
	}

	public void setVoltMaxFreyNo3(BigDecimal voltMaxFreyNo3) {
		set("voltMaxFreyNo3", voltMaxFreyNo3);
	}

	/**
	 * 最高单体序号频率第4的频率
	 */
	public BigDecimal getVoltMaxFreyNo4() {
		return getBigDecimal("voltMaxFreyNo4");
	}

	public void setVoltMaxFreyNo4(BigDecimal voltMaxFreyNo4) {
		set("voltMaxFreyNo4", voltMaxFreyNo4);
	}

	/**
	 * 最高单体序号频率第5的频率
	 */
	public BigDecimal getVoltMaxFreyNo5() {
		return getBigDecimal("voltMaxFreyNo5");
	}

	public void setVoltMaxFreyNo5(BigDecimal voltMaxFreyNo5) {
		set("voltMaxFreyNo5", voltMaxFreyNo5);
	}

	/**
	 * 最高单体序号频率第6的频率
	 */
	public BigDecimal getVoltMaxFreyNo6() {
		return getBigDecimal("voltMaxFreyNo6");
	}

	public void setVoltMaxFreyNo6(BigDecimal voltMaxFreyNo6) {
		set("voltMaxFreyNo6", voltMaxFreyNo6);
	}

	/**
	 * 最高单体序号频率第7的频率
	 */
	public BigDecimal getVoltMaxFreyNo7() {
		return getBigDecimal("voltMaxFreyNo7");
	}

	public void setVoltMaxFreyNo7(BigDecimal voltMaxFreyNo7) {
		set("voltMaxFreyNo7", voltMaxFreyNo7);
	}

	/**
	 * 最高单体序号频率第8的频率
	 */
	public BigDecimal getVoltMaxFreyNo8() {
		return getBigDecimal("voltMaxFreyNo8");
	}

	public void setVoltMaxFreyNo8(BigDecimal voltMaxFreyNo8) {
		set("voltMaxFreyNo8", voltMaxFreyNo8);
	}

	/**
	 * 最高单体序号频率第9的频率
	 */
	public BigDecimal getVoltMaxFreyNo9() {
		return getBigDecimal("voltMaxFreyNo9");
	}

	public void setVoltMaxFreyNo9(BigDecimal voltMaxFreyNo9) {
		set("voltMaxFreyNo9", voltMaxFreyNo9);
	}

	/**
	 * 最高单体序号频率第10的频率
	 */
	public BigDecimal getVoltMaxFreyNo10() {
		return getBigDecimal("voltMaxFreyNo10");
	}

	public void setVoltMaxFreyNo10(BigDecimal voltMaxFreyNo10) {
		set("voltMaxFreyNo10", voltMaxFreyNo10);
	}

	/**
	 * 电压一致性结果
	 */
	public String getVoltConsiyEval() {
		return getString("voltConsiyEval");
	}

	public void setVoltConsiyEval(String voltConsiyEval) {
		set("voltConsiyEval", voltConsiyEval);
	}

	/**
	 * 最高温度序号频率第1的编号
	 */
	public String getTempMaxNo1() {
		return getString("tempMaxNo1");
	}

	public void setTempMaxNo1(String tempMaxNo1) {
		set("tempMaxNo1", tempMaxNo1);
	}

	/**
	 * 最高温度序号频率第2的编号
	 */
	public String getTempMaxNo2() {
		return getString("tempMaxNo2");
	}

	public void setTempMaxNo2(String tempMaxNo2) {
		set("tempMaxNo2", tempMaxNo2);
	}

	/**
	 * 最高温度序号频率第3的编号
	 */
	public String getTempMaxNo3() {
		return getString("tempMaxNo3");
	}

	public void setTempMaxNo3(String tempMaxNo3) {
		set("tempMaxNo3", tempMaxNo3);
	}

	/**
	 * 最高温度序号频率第4的编号
	 */
	public String getTempMaxNo4() {
		return getString("tempMaxNo4");
	}

	public void setTempMaxNo4(String tempMaxNo4) {
		set("tempMaxNo4", tempMaxNo4);
	}

	/**
	 * 最高温度序号频率第5的编号
	 */
	public String getTempMaxNo5() {
		return getString("tempMaxNo5");
	}

	public void setTempMaxNo5(String tempMaxNo5) {
		set("tempMaxNo5", tempMaxNo5);
	}

	/**
	 * 最高温度序号频率第6的编号
	 */
	public String getTempMaxNo6() {
		return getString("tempMaxNo6");
	}

	public void setTempMaxNo6(String tempMaxNo6) {
		set("tempMaxNo6", tempMaxNo6);
	}

	/**
	 * 最高温度序号频率第7的编号
	 */
	public String getTempMaxNo7() {
		return getString("tempMaxNo7");
	}

	public void setTempMaxNo7(String tempMaxNo7) {
		set("tempMaxNo7", tempMaxNo7);
	}

	/**
	 * 最高温度序号频率第8的编号
	 */
	public String getTempMaxNo8() {
		return getString("tempMaxNo8");
	}

	public void setTempMaxNo8(String tempMaxNo8) {
		set("tempMaxNo8", tempMaxNo8);
	}

	/**
	 * 最高温度序号频率第9的编号
	 */
	public String getTempMaxNo9() {
		return getString("tempMaxNo9");
	}

	public void setTempMaxNo9(String tempMaxNo9) {
		set("tempMaxNo9", tempMaxNo9);
	}

	/**
	 * 最高温度序号频率第10的编号
	 */
	public String getTempMaxNo10() {
		return getString("tempMaxNo10");
	}

	public void setTempMaxNo10(String tempMaxNo10) {
		set("tempMaxNo10", tempMaxNo10);
	}

	/**
	 * 最高温度序号频率第1的频率
	 */
	public BigDecimal getTempMaxFreyNo1() {
		return getBigDecimal("tempMaxFreyNo1");
	}

	public void setTempMaxFreyNo1(BigDecimal tempMaxFreyNo1) {
		set("tempMaxFreyNo1", tempMaxFreyNo1);
	}

	/**
	 * 最高温度序号频率第2的频率
	 */
	public BigDecimal getTempMaxFreyNo2() {
		return getBigDecimal("tempMaxFreyNo2");
	}

	public void setTempMaxFreyNo2(BigDecimal tempMaxFreyNo2) {
		set("tempMaxFreyNo2", tempMaxFreyNo2);
	}

	/**
	 * 最高温度序号频率第3的频率
	 */
	public BigDecimal getTempMaxFreyNo3() {
		return getBigDecimal("tempMaxFreyNo3");
	}

	public void setTempMaxFreyNo3(BigDecimal tempMaxFreyNo3) {
		set("tempMaxFreyNo3", tempMaxFreyNo3);
	}

	/**
	 * 最高温度序号频率第4的频率
	 */
	public BigDecimal getTempMaxFreyNo4() {
		return getBigDecimal("tempMaxFreyNo4");
	}

	public void setTempMaxFreyNo4(BigDecimal tempMaxFreyNo4) {
		set("tempMaxFreyNo4", tempMaxFreyNo4);
	}

	/**
	 * 最高温度序号频率第5的频率
	 */
	public BigDecimal getTempMaxFreyNo5() {
		return getBigDecimal("tempMaxFreyNo5");
	}

	public void setTempMaxFreyNo5(BigDecimal tempMaxFreyNo5) {
		set("tempMaxFreyNo5", tempMaxFreyNo5);
	}

	/**
	 * 最高温度序号频率第6的频率
	 */
	public BigDecimal getTempMaxFreyNo6() {
		return getBigDecimal("tempMaxFreyNo6");
	}

	public void setTempMaxFreyNo6(BigDecimal tempMaxFreyNo6) {
		set("tempMaxFreyNo6", tempMaxFreyNo6);
	}

	/**
	 * 最高温度序号频率第7的频率
	 */
	public BigDecimal getTempMaxFreyNo7() {
		return getBigDecimal("tempMaxFreyNo7");
	}

	public void setTempMaxFreyNo7(BigDecimal tempMaxFreyNo7) {
		set("tempMaxFreyNo7", tempMaxFreyNo7);
	}

	/**
	 * 最高温度序号频率第8的频率
	 */
	public BigDecimal getTempMaxFreyNo8() {
		return getBigDecimal("tempMaxFreyNo8");
	}

	public void setTempMaxFreyNo8(BigDecimal tempMaxFreyNo8) {
		set("tempMaxFreyNo8", tempMaxFreyNo8);
	}

	/**
	 * 最高温度序号频率第9的频率
	 */
	public BigDecimal getTempMaxFreyNo9() {
		return getBigDecimal("tempMaxFreyNo9");
	}

	public void setTempMaxFreyNo9(BigDecimal tempMaxFreyNo9) {
		set("tempMaxFreyNo9", tempMaxFreyNo9);
	}

	/**
	 * 最高温度序号频率第10的频率
	 */
	public BigDecimal getTempMaxFreyNo10() {
		return getBigDecimal("tempMaxFreyNo10");
	}

	public void setTempMaxFreyNo10(BigDecimal tempMaxFreyNo10) {
		set("tempMaxFreyNo10", tempMaxFreyNo10);
	}

	/**
	 * 温度一致性结果
	 */
	public String getTempConsiyEval() {
		return getString("tempConsiyEval");
	}

	public void setTempConsiyEval(String tempConsiyEval) {
		set("tempConsiyEval", tempConsiyEval);
	}

	/**
	 * 电池包总容量
	 */
	public BigDecimal getCapacityBat() {
		return getBigDecimal("capacityBat");
	}

	public void setCapacityBat(BigDecimal capacityBat) {
		set("capacityBat", capacityBat);
	}

	/**
	 * 充电总电量
	 */
	public BigDecimal getChrgCapTotal() {
		return getBigDecimal("chrgCapTotal");
	}

	public void setChrgCapTotal(BigDecimal chrgCapTotal) {
		set("chrgCapTotal", chrgCapTotal);
	}


}
