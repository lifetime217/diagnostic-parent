package com.diagnostic.service;

import java.math.BigDecimal;
import java.util.List;

import com.alibaba.fastjson.JSONObject;

public interface ITCarWeekResultsService {
	
	/**
	 * 连线状态的数据
	 * @param
	 * @return
	 */
	public List<String> states(String vh);


	/**
	 * 充电行为综合评价图表的数据
	 * @param vh 车辆标识符
	 * @return
	 */
	public List<BigDecimal> chrgBehaviorEstimation(String vh);
	
	
	/**
	 * 最高电压频数图表的数据
	 * @param
	 * @return
	 */
	
	public JSONObject voltFreyModulecell (String vh);
	
	/**
	 * 最高温度频数图表的数据
	 * @param
	 * @return
	 */
	public JSONObject tempFreyNum (String vh);
	
	
	/**
	 * 月充电量/充电次数图表的数据
	 * @param
	 * @return
	 */
	public JSONObject monChrg(String vh);
	

	
	/**
	 * 月电池容量图表的数据
	 * @param
	 * @return
	 */
	public List<BigDecimal> batCapacity (String vh);
	

}
