package com.diagnostic.service;

import com.diagnostic.entity.TCarStartend;
import com.diagnostic.entity.TCarRuntime;

/**
 * 诊断数据接收的服务(开始和结束充电时的数据)
 *
 */
public interface IDiagnosticDataService {
	/**
	 * 充电开始的数据
	 * 
	 * @param info
	 * @return
	 */
	public Long chrgStart(TCarStartend info);

	/**
	 * 充电结束的数据
	 * 
	 * @param info
	 */
	public void chrgEnd(TCarStartend info);
	
	/**
	 * 充电中的数据
	 * 
	 * @param info
	 */
	public void chrgRuntime(TCarRuntime info);
}
